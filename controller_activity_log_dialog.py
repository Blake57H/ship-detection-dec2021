from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QDialog

import view_activity_log_dialog


class ActivityLogDialog(QDialog, view_activity_log_dialog.Ui_Dialog):

    def __init__(self, log_file:str , *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.log_file = log_file
        self.setWindowFlag(Qt.WindowType.WindowStaysOnTopHint)
        self.setupUi(self)
        with open(log_file, 'r', encoding='utf-8') as stream:
            self.plainTextEdit.setPlainText(''.join(stream.readlines()))

# def get_activity_log_dialog(log_file:str) -> ActivityLogDialog:
#     dialog = ActivityLogDialog(log_file)
#     ui = view_activity_log_dialog.Ui_Dialog
#     ui.setupUi(dialog)
#     return dialog
