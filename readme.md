# Boat detection program

Backup of work. It's gonna be useless anyway....

Look away.... onegai shimasu

I wanted it to be MVC style but looks like I have messed up.

## TODO list

- [half completed?] add support for a certain new type of input (stuck in reading RGB values)
- [completed] add theme function (background, icon and sticker)

## Known Issues (fix unscheduled)

- I don't own a macbook and therefore this program has not been tested in MacOS

- Unable to read tif image's projection under Linux. Probably has something to do with GDAL package installed in the test machine (Running Ubuntu 20.04.3).

- Did not set file filter on non-Windows machine when selecting a file (because I don't know how to do it).

## Feature to be added (unscheduled)

- A stop button to stop execution

## About license

I don't really know how license works. GPLv3 works if I open source this, I guess? Or should I use Apache 2.0?

And here include a trained neural network model (trained using yolo or something? Someone else does it, and I only did
the GUI). I dig a bit and it uses Apache License 2.0 license.

My usage of PySide6 are licensed under Open Source license (at least that's what I think).

## Change Log

### March 14, 2020

- Minor bug fix
