import pathlib
import re
import time
import xml.etree.ElementTree as eT
from typing import Union, Tuple, List

import math
import numpy as np
import tifffile
from PyQt6.QtCore import QObject, pyqtSignal
from cv2 import cv2
from tifffile import TiffPage
from osgeo import gdal, osr, ogr

from general_functions import show_warning, LanguagePack, SaveFormat, print_debug_message


# # PLP = progress language pack  # replaced by LanguagePack from general_functions
# class PLP(LanguagePack):
#     completed = 'Image preprocessing completed'
#     crop_image = 'Creating image segments'
#
#

# lp = LanguagePack  # moved to local variable

class ImageFileName:
    """
    may move file name format here
    trying to get naming convention and its regular expression match rule in one place
    """

    @staticmethod
    def get_name():
        pass

    @staticmethod
    def get_regex():
        return r'r(\d+)_c(\d+).'


# 16位转8位
def normalization(data):
    _range = np.max(data) - np.min(data)
    return (data - np.min(data)) / _range


def uint16_to_uint8(img_16):
    if np.max(img_16) - np.min(img_16) != 0:
        # img_nrm = (img_16 - np.min(img_16)) / (np.max(img_16) - np.min(img_16)) #计算灰度范围,归一化
        img_nrm = normalization(img_16)
        img_8 = np.uint8(255 * img_nrm)
    return img_8


def uint16_to_uint8_ver2(img_16, _drop_alpha: bool = False, quiet: bool = False):
    # print(np.min(img_16))
    if _drop_alpha:
        img_16 = img_16[:, :, :3]
    min_val = np.min(img_16)
    if min_val < 0:
        show_warning(f"WARNING: Min={min_val}<0, negative values will be set to 0")
        img_16[img_16 < 0] = 0
    max_val = np.max(img_16)
    if max_val <= 255:
        bit = 8
    elif max_val <= 1023:
        max_val = 1023
        bit = 10
    elif max_val <= 4095:
        max_val = 4095
        bit = 12
    else:
        # raise Exception(f"Unknown image bit-depth (max_val={max_val} > 12-bit), expected 8/10/12 bit")
        show_warning(f"WARNING: Max={max_val}>4095, highest value will be capped at 4095.")
        img_16[img_16 > 4095] = 4095
        bit = 12
    if bit > 8:
        if not quiet:
            print(f"Assuming {bit} bit color depth")
        # img_8 = np.array((img_16 / max_val * 255), dtype='uint8')
        return np.array((img_16 / max_val * 255), dtype='uint8')
    elif img_16.dtype == 'uint8':
        # img_8 = img_16
        return img_16
    else:
        # img_8 = np.array(img_16, dtype='uint8')
        return np.array(img_16, dtype='uint8')
    # print(np.max(img_8))
    # print(np.min(img_8))
    # print(img_8)
    # exit(0)
    # return img_8


# 图像颜色处理
def img_enhance(rgba, ratio=4, radius=2):  # rgb三通道分别增强，ratio是对比度增强因子，radius是卷积模板半径

    def stretchImage(data, s=0.01, bins=2000):  # 线性拉伸，去掉最大最小0.5%的像素值，然后线性拉伸至[0,1]
        ht = np.histogram(data, bins);
        d = np.cumsum(ht[0]) / float(data.size)
        lmin = 0
        lmax = bins - 1
        while lmin < bins:
            if d[lmin] >= s:
                break
            lmin += 1
        while lmax >= 0:
            if d[lmax] <= 1 - s:
                break
            lmax -= 1
        return np.clip((data - ht[1][lmin]) / (ht[1][lmax] - ht[1][lmin]), 0, 1)

    g_para = {}

    def getPara(radius=5):  # 根据半径计算权重参数矩阵
        # global g_para
        m = g_para.get(radius, None)
        if m is not None:
            return m
        size = radius * 2 + 1
        m = np.zeros((size, size))
        for h in range(-radius, radius + 1):
            for w in range(-radius, radius + 1):
                if h == 0 and w == 0:
                    continue
                m[radius + h, radius + w] = 1.0 / math.sqrt(h ** 2 + w ** 2)
        m /= m.sum()
        g_para[radius] = m
        return m

    def zmIce(I, ratio=4, radius=300):  # 常规的ACE实现
        para = getPara(radius)
        height, width = I.shape
        zh, zw = [0] * radius + list(range(height)) + [height - 1] * radius, [0] * radius + list(range(width)) + [
            width - 1] * radius
        Z = I[np.ix_(zh, zw)]
        res = np.zeros(I.shape)
        for h in range(radius * 2 + 1):
            for w in range(radius * 2 + 1):
                if para[h][w] == 0:
                    continue
                res += (para[h][w] * np.clip((I - Z[h:h + height, w:w + width]) * ratio, -1, 1))
        return res

    def zmIceFast(I, ratio, radius):  # 单通道ACE快速增强实现
        height, width = I.shape[:2]
        if min(height, width) <= 2:
            return np.zeros(I.shape) + 0.5
        Rs = cv2.resize(I, ((width + 1) // 2, (height + 1) // 2))
        Rf = zmIceFast(Rs, ratio, radius)  # 递归调用
        Rf = cv2.resize(Rf, (width, height))
        Rs = cv2.resize(Rs, (width, height))

        return Rf + zmIce(I, ratio, radius) - zmIce(Rs, ratio, radius)

    # r, g, b = rgba[:, :, 0], rgba[:, :, 1], rgba[:, :, 2]
    # img = np.dstack([b, g, r])
    img = np.dstack([rgba[:, :, 0], rgba[:, :, 1], rgba[:, :, 2]])

    img = img / 255.0
    res = np.zeros(img.shape)
    # 自动彩色均衡
    for k in range(3):
        res[:, :, k] = stretchImage(zmIceFast(img[:, :, k], ratio, radius))

    res = res[:, :, ::-1]

    # 伽马变换
    img_gamma = np.power(res, 0.75) * 255.0
    img_gamma = img_gamma.astype(np.uint8)

    return img_gamma


def tiff2png(img_16):
    # img_8 = uint16_to_uint8(img_16)
    img_8 = uint16_to_uint8_ver2(img_16)
    rgb = img_enhance(img_8)
    return rgb


def drop_alpha(rgba):
    return np.dstack([rgba[:, :, 0], rgba[:, :, 1], rgba[:, :, 2]])


def transpose(array, order):
    return np.transpose(array, order)


def auto_transpose(array: np.array, shape_structure: List[str]) -> np.array:
    """
    given the original order of height, width and channel, transpose the array into order [height, width, channel]
    """
    if shape_structure is None:
        shape_structure = ['h', 'w', 'c']
    elif len(shape_structure) != 3:
        raise ValueError("Invalid shape structure length")
    elif 'h' not in shape_structure or 'w' not in shape_structure or 'c' not in shape_structure:
        raise ValueError("Incorrect shape structure content")

    if shape_structure != ['h', 'w', 'c']:
        order = [0, 1, 2]
        for d in shape_structure:
            if d == 'h':
                order[0] = shape_structure.index(d)
            elif d == 'w':
                order[1] = shape_structure.index(d)
            elif d == 'c':
                order[2] = shape_structure.index(d)
        return transpose(array, (order[0], order[1], order[2]))
    return array


def str_to_pathlib(path: Union[str, pathlib.Path]) -> pathlib.Path:
    if isinstance(path, pathlib.Path):
        return path
    elif isinstance(path, str):
        return pathlib.Path(path)
    else:
        raise TypeError(f"Unrecognized type {type(path)}, expect str or pathlib.Path")


class GeoTransformer:
    def __init__(self, xml_path: pathlib.Path,
                 width_and_height: Tuple[int, int] = None,
                 ):
        """
        Parameters
        ----------

        xml_path: pathlib.Path()
            path to the xml file(tested), or path to the tif image (expecting xml file next to it)

        width_and_height: (int, int)
            image width and height, in pixel
        """
        self.init_message = ''
        self.tl = None
        self.tr = None
        self.bl = None
        self.br = None
        self.resolution = None
        self.geo_transform = None
        if self.xml_init(xml_path, width_and_height):
            pass
        else:
            # read image size from tifffile, and geo transform from GDAL (maybe incorrect)
            self.default_init(xml_path, width_and_height)

    def default_init(self, xml_path: pathlib.Path,
                     width_and_height: Tuple[int, int] = None
                     ) -> None:
        # tif = tifffile.TiffFile(xml_path.__str__())
        gdal_tif: gdal.Dataset = gdal.Open(xml_path.__str__())
        # self.resolution = (tif.pages[0].tags.valueof('ImageWidth'), tif.pages[0].tags.valueof('ImageLength'))
        self.resolution = (int(gdal_tif.RasterXSize), int(gdal_tif.RasterYSize))
        self.geo_transform = gdal_tif.GetGeoTransform()
        self.tl = (self.geo_transform[0], self.geo_transform[3])
        self.tr = self.pixel_to_decimal_geo(self.resolution[0], 0)
        self.bl = self.pixel_to_decimal_geo(0, self.resolution[1])
        self.br = self.pixel_to_decimal_geo(self.resolution[0], self.resolution[1])
        source_projection = osr.SpatialReference(wkt=gdal_tif.GetProjection()).GetAttrValue("AUTHORITY", 1)
        if source_projection is None:
            # todo cannot load geo coordinate correctly, unknown source projection
            # todo translate?
            self.init_message += "GeoTransformer: Unable to read projection, value is None\n"
        elif source_projection != '4326' and str.isdigit(source_projection):
            source_projection = int(source_projection)
            self.tl = self.geo_coordinate_conversion(self.tl, source_projection)
            self.tr = self.geo_coordinate_conversion(self.tr, source_projection)
            self.bl = self.geo_coordinate_conversion(self.bl, source_projection)
            self.br = self.geo_coordinate_conversion(self.br, source_projection)
            gt = np.zeros(6)
            gt[0], gt[3] = self.tl
            gt[1] = ((self.tr[0] - self.tl[0]) + (self.br[0] - self.bl[0])) / 2 / self.resolution[0]
            gt[2] = ((self.bl[0] - self.tl[0]) + (self.br[0] - self.tr[0])) / 2 / self.resolution[1]
            gt[4] = ((self.tr[1] - self.tl[1]) + (self.br[1] - self.bl[1])) / 2 / self.resolution[0]
            gt[5] = ((self.bl[1] - self.tl[1]) + (self.br[1] - self.tr[1])) / 2 / self.resolution[1]
            self.geo_transform = gt

    def xml_init(self, xml_path: pathlib.Path,
                 width_and_height: Tuple[int, int] = None,
                 ) -> bool:
        if xml_path.suffix != '.xml':
            xml_path = pathlib.Path(xml_path.parent).joinpath(xml_path.stem + '.xml')

        try:
            self._check_file(xml_path)
        except FileNotFoundError or IsADirectoryError:
            return False

        # read the corners' coordinate
        # tutorial for parsing xml: https://stackoverflow.com/questions/1912434/how-to-parse-xml-and-count-instances-of-a-particular-node-attribute
        # note: if xml is untrusted (means you are not 100% confident that the xml file is safe), use 'defusedxml',
        # and read more here: https://docs.python.org/3/library/xml.html#xml-vulnerabilities. In my case I simply
        # don't care.
        root = eT.parse(xml_path).getroot()
        self.tl = (float(root.findtext('TopLeftLongitude')), float(root.findtext('TopLeftLatitude')))
        self.tr = (float(root.findtext('TopRightLongitude')), float(root.findtext('TopRightLatitude')))
        self.bl = (float(root.findtext('BottomLeftLongitude')), float(root.findtext('BottomLeftLatitude')))
        self.br = (float(root.findtext('BottomRightLongitude')), float(root.findtext('BottomRightLatitude')))

        # read image resolution (in pixel)
        if width_and_height is None:
            width_and_height = (int(root.findtext('WidthInPixels')), int(root.findtext('HeightInPixels')))

        # calculate geo transform (I think....)
        # given there are two sets of values use the average as final geo transform value (idk, feeling like it)
        gt = np.zeros(6)
        gt[0], gt[3] = self.tl
        gt[1] = ((self.tr[0] - self.tl[0]) + (self.br[0] - self.bl[0])) / 2 / width_and_height[0]
        gt[2] = ((self.bl[0] - self.tl[0]) + (self.br[0] - self.tr[0])) / 2 / width_and_height[1]
        gt[4] = ((self.tr[1] - self.tl[1]) + (self.br[1] - self.bl[1])) / 2 / width_and_height[0]
        gt[5] = ((self.bl[1] - self.tl[1]) + (self.br[1] - self.tr[1])) / 2 / width_and_height[1]
        self.geo_transform = gt
        self.resolution = width_and_height
        return True

    def __str__(self):
        if self.resolution is not None:
            return (f"\n"
                    f"Corner coordinates:\n"
                    f"\tTop left: {self.tl}\n"
                    f"\tTop right: {self.tr}\n"
                    f"\tBottom left: {self.bl}\n"
                    f"\tBottom right: {self.br}\n"
                    f"Geo transform:\n"
                    f"\t{self.geo_transform}\n"
                    f"Image resolution (width * height):\n"
                    f"\t{self.resolution[0]}x{self.resolution[1]}")
        else:
            return (f"\n"
                    f"Corner coordinates:\n"
                    f"\tTop left: {self.tl}\n"
                    f"\tTop right: {self.tr}\n"
                    f"\tBottom left: {self.bl}\n"
                    f"\tBottom right: {self.br}\n"
                    f"Geo transform:\n"
                    f"\t{self.geo_transform}\n"
                    f"Image resolution (width * height):\n"
                    f"\t{self.resolution}")

    @staticmethod
    def _check_file(xml_path: Union[str, pathlib.Path]) -> None:
        """
        check given file:
            (1) exists
            (2) is not a path
        """
        if not pathlib.Path(xml_path).exists():
            raise FileNotFoundError(f"xml file not found: {xml_path}")
        if not pathlib.Path(xml_path).is_file():
            raise IsADirectoryError(f"{xml_path} is a directory")

    @staticmethod
    def geo_coordinate_conversion(
            coordinate: Tuple[float, float],
            source_projection: int,
            target_projection: int = 4326
    ) -> Tuple[float, float]:
        """
        convert coordinate with EPSG (default WGS84/Geographic)
        4326 = WGS84/Geographic

        Parameter
        ---------
        coordinate: Tuple[float, float]
            longitude, latitude (just pass "self.tl", "self.tr" and so on)
        source_projection: int
            source EPSG reference
        target_projection: int
            target EPSG reference

        Returns
        -------
        coordinate: Tuple[float, float]
            longitude, latitude
        """
        proj_source = osr.SpatialReference()
        proj_source.ImportFromEPSG(source_projection)
        proj_target = osr.SpatialReference()
        proj_target.ImportFromEPSG(target_projection)
        proj_target.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
        point = ogr.Geometry(ogr.wkbPoint)
        point.AddPoint(coordinate[0], coordinate[1])  # use your coordinates here
        point.AssignSpatialReference(proj_source)  # tell the point what coordinates it's in
        point.TransformTo(proj_target)  # project it to the out spatial reference
        print('{0},{1}'.format(point.GetX(), point.GetY()))  # output projected X and Y coordinates
        return point.GetX(), point.GetY()

    def get_geo_transform(self) -> Tuple[float]:
        return self.geo_transform

    def pixel_to_decimal_geo(self, x: int, y: int) -> Tuple[float, float]:
        """
        https://gdal.org/user/raster_data_model.html#affine-geotransform
        Xgeo = GT(0) + Xpixel*GT(1) + Yline*GT(2)
        Ygeo = GT(3) + Xpixel*GT(4) + Yline*GT(5)

        x_geo = longitude, y_geo = latitude
        """
        x_geo: float = self.geo_transform[0] + x * self.geo_transform[1] + y * self.geo_transform[2]
        y_geo: float = self.geo_transform[3] + x * self.geo_transform[4] + y * self.geo_transform[5]
        return x_geo, y_geo

    def decimal_geo_to_pixel(self, x_geo: float, y_geo: float) -> Tuple[int, int]:
        """
        https://gdal.org/user/raster_data_model.html#affine-geotransform
        Xgeo = GT(0) + Xpixel*GT(1) + Yline*GT(2)
        Ygeo = GT(3) + Xpixel*GT(4) + Yline*GT(5)

        x_geo = longitude, y_geo = latitude
        """
        gt = self.geo_transform

        # SAY GT(2) = a*GT(5)
        # Xgeo - Ygeo*a = GT(0) + Xpixel*GT(1) + Yline*GT(2) - a*(GT(3) + Xpixel*GT(4) + Yline*GT(5))
        # SAY Xgeo - Ygeo*a AS var_x,
        # AND GT(0) + Xpixel*GT(1) + Yline*GT(2) - a*(GT(3) + Xpixel*GT(4) + Yline*GT(5)) AS var_y
        # THEN var_x = var_y
        # MOVE GT0, GT3 TO THE LEFT, I GET: var_x - GT(0) + a*GT(3)
        #   = (Xpixel*GT(1) + Yline*GT(2)) - a*(Xpixel*GT(4) + Yline*GT(5))
        #   = Xpixel*GT(1) + Yline*GT(2) - a*Xpixel*GT(4) - a*Yline*GT(5)
        #   = Xpixel*GT(1) - a*Xpixel*GT(4)  [because Yline*GT(2) - a*Yline*GT(5) = 0]
        # GROUP: Xpixel * (GT(1) - a*GT(4)) =  Xgeo - Ygeo*a - GT(0) + a*GT(3)
        # FINALLY: Xpixel = (Xgeo - Ygeo*a - GT(0) + a*GT(3)) / (GT(1) - a*GT(4))

        a = gt[2] / gt[5]
        x_pixel = (x_geo - y_geo * a - gt[0] + a * gt[3]) / (gt[1] - a * gt[4])
        y_line = (y_geo - gt[3] - x_pixel * gt[4]) / gt[5]
        y_line2 = (x_geo - gt[0] - x_pixel * gt[1]) / gt[2]
        print(f"{x_pixel}; {y_line}; {y_line2}")  # debug todo: check both y_line's value same
        return x_pixel, y_line

    def get_width(self) -> int:
        return self.resolution[0]

    def get_height(self) -> int:
        return self.resolution[1]

    def get_resolution_str(self) -> str:
        """
        resolution = (width, height)
        """
        return f"{self.resolution[0]}x{self.resolution[1]}"

    def is_ready(self) -> bool:
        return self.resolution is not None and self.geo_transform is not None

    def pixel_to_decimal_geo_with_filename(self, filename: str, x_pixel: int, y_pixel: int,
                                           segment_size: int) -> Tuple[float, float]:
        match = re.search(ImageFileName.get_regex(), filename.__str__())
        l = re.compile(r'\d+').findall(match.group())
        assert len(l) == 2
        return self.pixel_to_decimal_geo(int(l[0]) * segment_size + x_pixel, int(l[1]) * segment_size + y_pixel)


def get_cropped(page: TiffPage,
                start_width: int,
                start_height: int,
                crop_size_width: int,
                crop_size_height: int,
                formatted_page_shape: tuple,
                shape_structure: List[str] = None,
                bypass_error: bool = True) -> np.array:
    """

    Parameters
    ----------

    Returns
    -------
    """

    v = []  # will be used in cropping image
    o = []
    height, width, channels = formatted_page_shape
    # print(formatted_page_shape)

    if start_height < 0 or \
            start_height + crop_size_height > height \
            or start_width < 0 or \
            start_width + crop_size_width > width:
        if bypass_error:
            start_height = max(0, start_height)
            start_width = max(0, start_width)
            crop_size_height = min(height - start_height, crop_size_height)
            crop_size_width = min(width - start_width, crop_size_width)
        else:
            raise ValueError("Requested crop area is out of image bounds.")

    if shape_structure is None:
        shape_structure = ['h', 'w', 'c']
    elif len(shape_structure) != 3:
        raise ValueError("Invalid shape structure length")
    elif 'h' not in shape_structure or 'w' not in shape_structure or 'c' not in shape_structure:
        raise ValueError("Incorrect shape structure content")

    order = [0, 1, 2]
    for d in shape_structure:
        if d == 'h':
            v.append(start_height)
            v.append(start_height + crop_size_height)
            order[0] = shape_structure.index(d)
        elif d == 'w':
            v.append(start_width)
            v.append(start_width + crop_size_width)
            order[1] = shape_structure.index(d)
        elif d == 'c':
            v.append(0)
            v.append(channels)
            order[2] = shape_structure.index(d)
    # print(order)

    _test = page.asarray('memmap')  # use memmap to avoid memory overflow, especially for really large tiff image

    # tiff image for the project I'm involved shapes (height, width, channel)
    save = _test[v[0]:v[1], v[2]:v[3], v[4]:v[5]]
    if shape_structure != ['h', 'w', 'c']:
        save = np.transpose(save, (order[0], order[1], order[2]))

    # print(f"DEBUG: {page.shape} -> {save.shape}")
    # exit(0)

    return np.array(save)


def get_cropped_alt(page: Tuple[int, int, int],
                    start_width: int,
                    start_height: int,
                    crop_size_width: int,
                    crop_size_height: int,
                    formatted_page_shape: Tuple[int, int, int],
                    shape_structure: List[str] = None,
                    bypass_error: bool = True) -> np.array:
    """

    Parameters
    ----------
    page: Tuple[int, int, int]
        3 dimensional array representing an image, dimension order is [height, width, channel]

    start_width: int
        left pixel position where the crop area is

    start_height: int
        top pixel position where the crop area is

    crop_size_width: int
        crop area width

    crop_size_height: int
        crop area height

    formatted_page_shape: (int, int, int)
        refer to "format_shape_structure(tif_page: TiffPage) -> (int, int, int)"

    shape_structure: List[str]
        original array dimensional order? a lttle hard to explain
        if image's dimension order is [channel, height, width], then it will be ['c', 'h', 'w']
        if image's dimension order is [height, width, channel], then it will be ['h', 'w', 'c']
        hope you'll get it
        ['h', 'w', 'c'] is the default value

    bypass_error: bool
        When True, if crop size is bigger than image's remaining size (which will produce image out of bound exception),
        remaining size will be used to avoid exception. Default on (True).
        If one expecting all cropped image are the same fixed resolution, turn it off. Remember to add exception
        handling.

    Returns
    -------
        3 dimensional numpy array representing the cropped image
    """

    height, width, channels = formatted_page_shape
    # print(formatted_page_shape)

    if start_height < 0 or \
            start_height + crop_size_height > height or \
            start_width < 0 or \
            start_width + crop_size_width > width:
        if bypass_error:
            start_height = max(0, start_height)
            start_width = max(0, start_width)
            crop_size_height = min(height - start_height, crop_size_height)
            crop_size_width = min(width - start_width, crop_size_width)
        else:
            raise ValueError("Requested crop area is out of image bounds.")

    return page[start_height:start_height + crop_size_height, start_width:start_width + crop_size_width, :]


def format_shape_structure(tif_page: TiffPage) -> Tuple[int, int, int]:
    """
    return dimensional order of an image array's height, width and channel

    e.g.:
    if given array's dimension order is [height, width, channel], then return [0,1,2]

    if given array's dimension order is channel, height, width (as in GF6 data)
    [channel, height, width] -> height at dimension 1, width at dimension 2, channel at dimension 0 -> [1,2,0]
    """

    height, width, channels = tif_page.shape
    height_value = tif_page.tags['ImageLength'].value
    width_value = tif_page.tags['ImageWidth'].value
    # note, by the processing logic, if height equals width, then it will assume height first then width
    shape_structure_dict = {}

    if height_value != height:
        if width_value == height:
            shape_structure_dict['w'] = 0
        if height_value == width:
            shape_structure_dict['h'] = 1
            if shape_structure_dict.get('w') is None:
                shape_structure_dict['w'] = 2
        elif height_value == channels:
            shape_structure_dict['h'] = 2
            if shape_structure_dict.get('w') is None:
                shape_structure_dict['w'] = 1
    else:
        shape_structure_dict['h'] = 0
        if width_value != width:
            shape_structure_dict['w'] = 2
        else:
            shape_structure_dict['w'] = 1
    shape_structure_dict['c'] = 3 - shape_structure_dict['h'] - shape_structure_dict['w']
    return shape_structure_dict['h'], shape_structure_dict['w'], shape_structure_dict['c']


def _warning(
        message,
        category=UserWarning,
        filename='',
        lineno=-1,
        file=None,
        line=None):
    print(message)


def save_as(filepath: pathlib.Path, _type: SaveFormat, _array: np.array, is_bgr: bool = False):
    """
    Save an array as image
    :param filepath: file path, pathlib.Path
    :param _type: image format, refer to SaveFormat
    :param _array: the array to save as image, RGB (NOT BGR!!)
    :param is_bgr: as it says
    :return: nothing
    """
    if filepath.exists():
        if filepath.is_file():
            pass
            show_warning(f"{filepath.__str__()} exists and will be over-written")
        else:
            raise FileExistsError(f"a directory named {filepath.__str__()} exists, remove it before continuing")

    if not filepath.parent.exists():
        filepath.parent.mkdir(parents=True)

    if _type == SaveFormat.PNG:
        if is_bgr:
            cv2.imwrite(filepath.__str__(), _array, [int(cv2.IMWRITE_PNG_COMPRESSION), 9])
            return
        cv2.imwrite(filepath.__str__(), cv2.cvtColor(_array, cv2.COLOR_RGB2BGR), [int(cv2.IMWRITE_PNG_COMPRESSION), 9])
        # png.from_array(_array, color).save(filepath.__str__())
    elif _type == SaveFormat.TIFF:
        tifffile.imwrite(filepath.__str__(), _array)
    elif _type == SaveFormat.JPG:
        if is_bgr:
            cv2.imwrite(filepath.__str__(), _array, [int(cv2.IMWRITE_JPEG_QUALITY), 100])
            return
        cv2.imwrite(filepath.__str__(), cv2.cvtColor(_array, cv2.COLOR_RGB2BGR), [int(cv2.IMWRITE_JPEG_QUALITY), 100])
        # cv2.imwrite(filepath.__str__(), _array, [int(cv2.IMWRITE_JPEG_QUALITY), 100])
    else:
        raise ValueError("Unrecognized type, use options offered in class SaveFormat")


class ImageCropper:
    def __init__(self,
                 crop_width: int,
                 crop_height: int,
                 output_folder: pathlib.Path,
                 output_format: SaveFormat,
                 sub_directory_count: int = 0,
                 filename_postfix: str = None
                 ):
        self.crop_width = crop_width
        self.crop_height = crop_height
        self.output_folder = output_folder
        self.output_format = output_format
        self.sub_directory_count = sub_directory_count
        self._filename_postfix = filename_postfix

    @staticmethod
    def __swap_variable(var1, var2):
        """
        a simple method returning swapped variable

        Parameters
        ----------
            var1, var2

        Returns
        -------
            var2, var1
        """
        return var2, var1

    @staticmethod
    def __swap_array(array, index1, index2):
        array[index1], array[index2] = array[index2], array[index1]
        return array

    def set_output_folder(self, path: pathlib.Path):
        self.output_folder = path

    def set_filename_postfix(self, postfix: str):
        self._filename_postfix = postfix

    def get_postfix(self) -> str:
        if self._filename_postfix is None:
            return ''
        return f"_{self._filename_postfix}"

    def execute(self, tif_page: TiffPage, crop_only: bool = False, sub_directory_count: int = 0,
                enhance_first: bool = False):
        """
        Parameters
        ----------

        tif_page: TiffPage
            A page from tif

        crop_only: bool
            If true, crop image without depth scaling or

        Returns
        -------
        out
            i donno, maybe no return
        """
        if len(tif_page.shape) != 3:
            raise ValueError("tif page must be shaped like a 3d array")

        height, width, channels = tif_page.shape
        height_value = tif_page.tags['ImageLength'].value
        width_value = tif_page.tags['ImageWidth'].value

        shape_structure = ['o', 'o', 'o']

        _h, _w, _c = format_shape_structure(tif_page)
        """        
        shape_structure_dict = {}

        if height_value != height:
            if width_value == height:
                shape_structure_dict['w'] = 0
            if height_value == width:
                shape_structure_dict['h'] = 1
                if shape_structure_dict.get('w') is None:
                    shape_structure_dict['w'] = 2
                # height, width = self.__swap_variable(height, width)
            elif height_value == channels:
                shape_structure_dict['h'] = 2
                if shape_structure_dict.get('w') is None:
                    shape_structure_dict['w'] = 1
                # height, channels = self.__swap_variable(height, channels)
        else:
            shape_structure_dict['h'] = 0
            if width_value != width:
                # width, channels = self.__swap_variable(width, channels)
                shape_structure_dict['w'] = 2
            else:
                shape_structure_dict['w'] = 1
        shape_structure_dict['c'] = 3 - shape_structure_dict['h'] - shape_structure_dict['w']
        """

        shape_structure[_h] = 'h'
        shape_structure[_w] = 'w'
        shape_structure[_c] = 'c'

        fps = [height, width, channels]  # fps = Formatted Page Shape
        '''
        height, width, channels = \
            fps[shape_structure_dict['h']], fps[shape_structure_dict['w']], fps[shape_structure_dict['c']]
        '''
        height, width, channels = fps[_h], fps[_w], fps[_c]
        fps = (height, width, channels)

        expected_segment_count = math.ceil(height / self.crop_height) * math.ceil(width / self.crop_width)
        tiles_per_dir = expected_segment_count
        if expected_segment_count < 30:
            print_per_segment = 1
        else:
            print_per_segment = 10
        segment = 0
        sub_directory_index = 1
        if sub_directory_count > 0:
            tiles_per_dir = math.ceil(expected_segment_count / sub_directory_count)
            self.output_folder = self.output_folder.joinpath(sub_directory_index.__str__())
        print(f"Expecting {expected_segment_count} segment(s)")
        '''
        if sub_directory_count > 0:
            tiles_per_dir = math.ceil(expected_segment_count/tiles_per_dir)
        '''
        row_index = column_index = 0
        while row_index * self.crop_height < height:
            image = get_cropped(
                page=tif_page,
                start_width=column_index * self.crop_width,
                start_height=row_index * self.crop_height,
                crop_size_width=self.crop_width,
                crop_size_height=self.crop_height,
                formatted_page_shape=fps,
                shape_structure=shape_structure
            )
            image = uint16_to_uint8_ver2(image, _drop_alpha=True)
            # print(image.shape)
            if not crop_only:
                image = img_enhance(image)
            # print(image.shape)
            filename = f"r{row_index}_c{column_index}{self.get_postfix()}{self.output_format}"
            save_as(_array=image, _type=self.output_format, filepath=self.output_folder.joinpath(filename))
            segment += 1
            if segment % print_per_segment == 0:
                print(f"\r{round(segment * 100 / expected_segment_count, ndigits=1)}% complete", end='')
            if segment % tiles_per_dir == 0:
                sub_directory_index += 1
                self.output_folder = self.output_folder.parent.joinpath(sub_directory_index.__str__())
            column_index += 1
            if column_index * self.crop_width >= width:
                column_index = 0
                row_index += 1
        print()


class ImageCropperReversed(QObject):
    """
    ImageCropper will crop image to the desired size then enhance the image
    ImageCropperReversed will enhance the whole image first, then crop the enhanced image
    """

    # for convenience the 'object' will be Tuple[str, int]. Pass None if there is no need to update a certain value.
    update_progress_callback = pyqtSignal(str, int)

    def __init__(self, crop_width: int, crop_height: int, output_folder: pathlib.Path or None,
                 output_format: SaveFormat, sub_directory_count: int = 0, filename_postfix: str = None,
                 update_callback_signal=None, language_pack: LanguagePack = None, memmap: bool = False):
        super().__init__()
        self.crop_width = crop_width
        self.crop_height = crop_height
        self.output_folder = output_folder
        self.output_format = output_format
        self.sub_directory_count = sub_directory_count
        self._filename_postfix = filename_postfix
        self._callback_connected = False
        self.lp = LanguagePack if language_pack is None else language_pack
        self.memmap = memmap

    def emit_progress(self, message: str or None, progress: int or None):
        self.update_progress_callback.emit(message, progress)

    def progress_manager(self):
        pass

    @staticmethod
    def __swap_variable(var1, var2):
        """
        a simple method returning swapped variable

        Parameters
        ----------
            var1, var2

        Returns
        -------
            var2, var1
        """
        return var2, var1

    @staticmethod
    def __swap_array(array, index1, index2):
        array[index1], array[index2] = array[index2], array[index1]
        return array

    def set_output_folder(self, path: pathlib.Path):
        self.output_folder = path

    def set_filename_postfix(self, postfix: str):
        self._filename_postfix = postfix

    def get_postfix(self) -> str:
        if self._filename_postfix is None:
            return ''
        return f"_{self._filename_postfix}"

    def execute(self, tif_page: TiffPage, crop_only: bool = False, sub_directory_count: int = 0):
        """
        todo: perhaps change input from 'TiffPage' to 'np.array'? It will also change format_shape_structure's logic.

        Parameters
        ----------

        tif_page: TiffPage
            A page from tif

        crop_only: bool
            If true, crop image without depth scaling or

        sub_directory_count: int
            If value greater than zero, given number of subdirectory will be created and cropped image will be split
            evenly in them.

        Returns
        -------
        out
            i donno, maybe no return
        """
        if len(tif_page.shape) != 3:
            raise ValueError("tif page must be shaped like a 3d array")

        self.emit_progress(self.lp.read_image, 0)

        height, width, channels = tif_page.shape

        shape_structure = ['o', 'o', 'o']

        _h, _w, _c = format_shape_structure(tif_page)  # get the dimensional order of that image

        shape_structure[_h] = 'h'
        shape_structure[_w] = 'w'
        shape_structure[_c] = 'c'

        fps = [height, width, channels]  # fps = Formatted Page Shape
        height, width, channels = fps[_h], fps[_w], fps[_c]
        fps = (height, width, channels)  # now height, width and channel are in correct order

        expected_segment_count = math.ceil(height / self.crop_height) * math.ceil(width / self.crop_width)
        tiles_per_dir = expected_segment_count
        if expected_segment_count < 30:
            print_per_segment = 1
        else:
            print_per_segment = 10
        segment = 0
        sub_directory_index = 1
        if sub_directory_count > 0:
            tiles_per_dir = math.ceil(expected_segment_count / sub_directory_count)
            self.output_folder = self.output_folder.joinpath(sub_directory_index.__str__())
        print(f"Expecting {expected_segment_count} segment(s)")
        row_index = column_index = 0
        # noinspection SpellCheckingInspection
        if self.memmap:
            image = tif_page.asarray('memmap')
        else:
            image = tif_page.asarray()
        # print('sleeping')
        # time.sleep(20)
        image = auto_transpose(image, shape_structure)
        if not crop_only:
            print_debug_message("sleeping", sleep=20)

            image = uint16_to_uint8_ver2(image, _drop_alpha=True)

            self.emit_progress(self.lp.enhance_image, 10)

            image = img_enhance(image)

            self.emit_progress(self.lp.crop_image, 90)

            while row_index * self.crop_height < height:
                image1 = get_cropped_alt(
                    page=image,
                    start_width=column_index * self.crop_width,
                    start_height=row_index * self.crop_height,
                    crop_size_width=self.crop_width,
                    crop_size_height=self.crop_height,
                    formatted_page_shape=fps,
                    shape_structure=shape_structure
                )
                # print(image.shape)
                filename = f"r{row_index}_c{column_index}{self.get_postfix()}{self.output_format}"
                save_as(_array=image1, _type=self.output_format, filepath=self.output_folder.joinpath(filename))
                segment += 1
                if segment % print_per_segment == 0:
                    print(f"\r{round(segment * 100 / expected_segment_count, ndigits=1)}% complete", end='')
                    self.emit_progress('', 90 + int(segment * 10 / expected_segment_count))
                if segment % tiles_per_dir == 0:
                    sub_directory_index += 1
                    self.output_folder = self.output_folder.parent.joinpath(sub_directory_index.__str__())
                column_index += 1
                if column_index * self.crop_width >= width:
                    column_index = 0
                    row_index += 1
        else:
            print_debug_message("enter new cropping method without enhance")

            while row_index * self.crop_height < height:
                image1 = get_cropped_alt(
                    page=image,
                    start_width=column_index * self.crop_width,
                    start_height=row_index * self.crop_height,
                    crop_size_width=self.crop_width,
                    crop_size_height=self.crop_height,
                    formatted_page_shape=fps,
                    shape_structure=shape_structure
                )
                image1 = uint16_to_uint8_ver2(image1, _drop_alpha=True, quiet=True)
                # print(image.shape)
                filename = f"r{row_index}_c{column_index}{self.get_postfix()}{self.output_format}"
                save_as(_array=image1, _type=self.output_format, filepath=self.output_folder.joinpath(filename))
                segment += 1
                if segment % print_per_segment == 0:
                    print(f"\r{round(segment * 100 / expected_segment_count, ndigits=1)}% complete", end='')
                    self.emit_progress('', int(segment * 100 / expected_segment_count))
                if segment % tiles_per_dir == 0:
                    sub_directory_index += 1
                    self.output_folder = self.output_folder.parent.joinpath(sub_directory_index.__str__())
                column_index += 1
                if column_index * self.crop_width >= width:
                    column_index = 0
                    row_index += 1

        print()
        self.emit_progress('', 100)
        return


class ImageMerger:
    """
    Merge all processed image segment to a whole image.
    Currently, supports only 8-bit RGB (__init__ parameter are reserved for future development)
    bit_depth and channels should not be changed (otherwise behavior may unexpected)

    the whole image processing uses library OpenCV, therefore color in numpy array will be BGR.
    """

    def __init__(self, image_width: int, image_height: int, tile_size: int, bit_depth: int = 8, channels: int = 3):
        if bit_depth == 8:
            # noinspection SpellCheckingInspection
            dtype = 'uint8'
        else:
            raise ValueError("bit depth other than 8 isn't supported")

        self.image_array = np.zeros((image_height, image_width, channels), dtype=dtype)
        self.tile_size = tile_size

    def _array_element_replace(self, start_height: int, start_width: int, replacing_array: np.ndarray):
        """
        Parameters
        ----------
        replacing_array: np.ndarray
            segment of an image, expecting dimension order [height, width, channel]
        """
        size = replacing_array.shape
        self.image_array[start_height:start_height + size[0], start_width:start_width + size[1], :] = replacing_array

    def add_image_tile(self, image_filepath: Union[pathlib.Path, str]):
        """
        add a tile to image array
        """
        image_filepath = str_to_pathlib(image_filepath)
        naming_rule = r'r(\d+)_c(\d+).'  # todo may move this regular expression to a class
        match = re.search(naming_rule, image_filepath.name)
        if match is None:
            show_warning(f"image file name {image_filepath.name} is not valid and will be ignored.")
            return
        something = re.compile(r'\d+').findall(match.group())
        assert len(something) == 2, f"expecting 2 elements in {something}"
        row = int(something[0])
        column = int(something[1])
        self._array_element_replace(row * self.tile_size, column * self.tile_size,
                                    cv2.imread(image_filepath.__str__(), cv2.IMREAD_COLOR))

    def save(self, save_path: Union[str, pathlib.Path], formats: SaveFormat = SaveFormat.JPG):
        save_path = str_to_pathlib(save_path)
        save_as(save_path, formats, self.image_array, is_bgr=True)


if __name__ == "__main__":
    test = np.random.rand(500, 500, 4)
    result = tiff2png(test)
    print(result.shape)
