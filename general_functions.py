import json
import os
import shutil
import time
import warnings
from datetime import datetime
from pathlib import Path
from typing import List, Dict, Optional


class SaveFormat:
    """
    Currently implemented image format to save
    """
    PNG = '.png'
    RGBA_PNG = '_rgba.png'
    TIFF = '.tiff'
    TIF = '.tif'
    JPG = '.jpg'
    CSV = '.csv'


# noinspection SpellCheckingInspection
class LanguagePack:
    program_ready = "Ready to select source"
    execute_ready = "Ready to run"
    executing = "Running"
    execute_complete = "Completed"

    read_geo_info = "Updating tiff info"
    reset_geo_info = "Cleaning front end tiff info"

    tif_image_loaded = "{} loaded"

    read_image = "Reading image"
    enhance_image = "Enhancing image"
    crop_image = 'Creating image segments'
    detecting_ships = 'Recognizing ships from image segments'
    displaying_result = "Loading detection result"
    no_result = "Detected no ship from this image"
    one_result = "Detected {} ship from this image"
    lots_of_result = "Detected {} ships form this image"

    error_popup_title = "An error has occurred"
    error_popup_body = "An error has occurred and program needs to reset:"
    error_no_image_to_save_title = "No image to save"
    error_no_image_to_save_body = "Image files may be deleted"

    info_no_theme_available = "No theme available"

    merge_image = "Merging image"
    save_image = "Saving image"

    exit_program = "Program Terminating"
    exit_program_canceled = "Program Termination Canceled"
    exit_program_confirm_title = "An image is processing"
    exit_program_confirm_text = "Confirm exit?"

    about_body = "GDUT :)"
    about_license = """
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
    """
    about_title = "About"

    def to_list(self) -> List[str]:
        return [
            self.program_ready,
            self.execute_ready,
            self.executing,
            self.execute_complete,

            self.read_geo_info,
            self.reset_geo_info,

            self.tif_image_loaded,

            self.read_image,
            self.enhance_image,
            self.crop_image,
            self.detecting_ships,
            self.displaying_result,
            self.no_result,
            self.one_result,
            self.lots_of_result,

            self.error_popup_title,
            self.error_popup_body,
            self.error_no_image_to_save_title,
            self.error_no_image_to_save_body,

            self.info_no_theme_available,

            self.merge_image,
            self.save_image,

            self.exit_program,
            self.exit_program_canceled,
            self.exit_program_confirm_title,
            self.exit_program_confirm_text,

            self.about_body,
            self.about_title
        ]

    def load_translation(self, file: Path):
        """
        future function perhaps
        """
        if file.exists() and file.is_file():
            with open(file, 'r', encoding="utf8") as stream:
                language: Dict = json.loads("".join(stream.readlines()))
        else:
            return  # avoid 'language undifined exception'
        self.program_ready = language.get(self.program_ready, self.program_ready)
        self.execute_ready = language.get(self.execute_ready, self.execute_ready)
        self.executing = language.get(self.executing, self.executing)
        self.execute_complete = language.get(self.execute_complete, self.execute_complete)

        self.read_geo_info = language.get(self.read_geo_info, self.read_geo_info)
        self.reset_geo_info = language.get(self.reset_geo_info, self.reset_geo_info)

        self.tif_image_loaded = language.get(self.tif_image_loaded, self.tif_image_loaded)

        self.read_image = language.get(self.read_image, self.read_image)
        self.enhance_image = language.get(self.enhance_image, self.enhance_image)
        self.crop_image = language.get(self.crop_image, self.crop_image)
        self.detecting_ships = language.get(self.detecting_ships, self.detecting_ships)
        self.displaying_result = language.get(self.displaying_result, self.displaying_result)
        self.no_result = language.get(self.no_result, self.no_result)
        self.one_result = language.get(self.one_result, self.one_result)
        self.lots_of_result = language.get(self.lots_of_result, self.lots_of_result)

        self.error_popup_title = language.get(self.error_popup_title, self.error_popup_title)
        self.error_popup_body = language.get(self.error_popup_body, self.error_popup_body)
        self.error_no_image_to_save_title = language.get(self.error_no_image_to_save_title, self.error_no_image_to_save_title)
        self.error_no_image_to_save_body = language.get(self.error_no_image_to_save_body, self.error_no_image_to_save_body)

        self.info_no_theme_available = language.get(self.info_no_theme_available, self.info_no_theme_available)

        self.merge_image = language.get(self.merge_image, self.merge_image)
        self.save_image = language.get(self.save_image, self.save_image)

        self.exit_program = language.get(self.exit_program, self.exit_program)
        self.exit_program_canceled = language.get(self.exit_program_canceled, self.exit_program_canceled)
        self.exit_program_confirm_title = language.get(self.exit_program_confirm_title, self.exit_program_confirm_title)
        self.exit_program_confirm_text = language.get(self.exit_program_confirm_text, self.exit_program_confirm_text)

        self.about_body = language.get(self.about_body, self.about_body)
        self.about_title = language.get(self.about_title, self.about_title)

    def save_translation(self):
        language = {}
        for item in self.to_list():
            language[item] = item
        with open('language/lang_out.json', 'w') as stream:
            stream.write(json.dumps(language))


def show_warning(message: str):
    # warnings.showwarning = _warning
    # warnings.warn(message)
    warnings.warn_explicit(message, UserWarning, '', -1)


def clean_temporary_folder(specific_directory: str):
    # noinspection GrazieInspection
    """
        Delete all folder and files in a directory (used to clear temporary files)
        You are allowed to delete other things as well
        :param specific_directory: the folder you wanna clean
        :return: nothing
    """
    if not os.path.exists(specific_directory):
        return
    assert os.path.isdir(specific_directory), f"{specific_directory} is not a directory"
    for filename in os.listdir(specific_directory):
        file_path = os.path.join(specific_directory, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))


def log_function(log_file: Path, message):
    if not log_file.parent.exists():
        log_file.parent.mkdir(parents=True)
    with open(log_file.__str__(), 'a', encoding='utf-8') as stream:
        stream.write(f"[{datetime.now().__str__()}] ")
        stream.writelines(message)
        stream.writelines('\n')


class LogFileName:
    string_format = "%Y-%m-%d %H-%M-%S %f"

    def get_time_as_filename(self) -> str:
        time = datetime.now().strftime(self.string_format)
        return time

    def parse_filename(self, file_path: Path) -> Optional[datetime]:
        if not file_path.is_file():
            return datetime.now()
        file_name = file_path.name[:-len(file_path.suffix)]
        try:
            return datetime.strptime(file_name, self.string_format)
        except:
            return None


def print_debug_message(message: str, log_file: Optional[Path] = None, sleep: Optional[int] = 0):
    message = f"Debug: {message}"
    print(message)
    if sleep > 0:
        time.sleep(sleep)
    if log_file is not None:
        log_function(log_file, message)
