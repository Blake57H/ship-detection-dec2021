import json
from pathlib import Path

from PyQt6 import QtCore
from PyQt6.QtCore import pyqtSignal


class ProgramSetting:
    """
    Read information from config.json
    Does not involve in applying settings (they are in iit_exec in controller)
    """
    _file = "config.json"
    # json element names
    _cuda = "cuda"
    _enhance_image = "enhance image"
    _language = "language"
    _theme = "theme"

    def __init__(self):
        self.cuda: bool = False
        self.enhance_image: bool = True
        self.language = ''
        self.theme = ''

    def to_json(self):
        settings = {
            # self._cuda: self.cuda,
            # self._enhance_image: self.enhance_image,
            self._language: self.language,
            self._theme: self.theme
        }
        with open(self._file, 'w') as stream:
            stream.write(json.dumps(settings))

    def from_json(self):
        settings_file = Path(self._file)
        if settings_file.exists() and settings_file.is_file():
            settings: dict
            with open(settings_file.__str__(), 'r') as stream:
                settings = json.loads("".join(stream.readlines()))
            self.cuda = settings.get(self._cuda, False)
            self.enhance_image = settings.get(self._enhance_image, False)
            self.language = settings.get(self._language, '')
            self.theme = settings.get(self._theme, '')

    def set_cuda(self, cuda: bool):
        # print(f'cuda={cuda}')
        self.cuda = cuda

    def get_cuda(self) -> bool:
        return self.cuda

    def set_enhance_image(self, enhance_image: bool):
        self.enhance_image = enhance_image

    def get_enhance_image(self) -> bool:
        return self.enhance_image

    def set_language(self, lang: str):
        language_path = Path("language/")
        display = language_path.joinpath(f"{lang}.qm")
        additional_text = language_path.joinpath(f"{lang}.json")
        # print(view_language.exists())
        # print(language_pack.exists())
        if display.exists() and additional_text.exists():
            self.language = lang

    def is_language_available(self) -> bool:
        return self.language != ''

    def get_display_language(self) -> str:
        return f"language/{self.language}.qm"

    def get_message_language(self) -> str:
        return f"language/{self.language}.json"

    def set_theme(self, theme: str):
        self.theme = theme

    def get_theme_dir(self, default_dir='theme') -> Path:
        return Path(default_dir)

    def get_theme_icon(self) -> str:
        """
        Return icon file path as string

        Returns
        -------
        icon file path : str
        """
        icon = self.get_theme_dir().joinpath(self.theme)
        file_list = icon.glob('icon.*')
        for item in file_list:
            return item.__str__() if item.is_file() else ''

    def get_theme_stylesheet(self) -> str:
        """
        Return background image as css string

        Returns
        -------
        background image (css) : str
        """
        stylesheet = ''

        # get background (image in QMainWindow)
        icon = self.get_theme_dir().joinpath(self.theme)
        file_list = icon.glob('background.*')
        for item in file_list:
            if not item.is_file():
                continue
            item = item.__str__().replace("\\", "/")
            stylesheet += \
                '#MainWindow {' \
                f'background-image: url("{item}"); ' \
                'background-repeat: no-repeat;' \
                'background-position: center;' \
                "} "
            break

        # get sticker (image in tab)
        file_list = icon.glob('sticker.*')
        for item in file_list:
            if not item.is_file():
                continue
            item = item.__str__().replace("\\", "/")
            stylesheet += \
                '#tab_info, #tab_config, #tab_result {' \
                f'background-image: url("{item}"); ' \
                'background-repeat: no-repeat;' \
                'background-position: right bottom;' \
                "} "
            break

        return stylesheet

    def __str__(self) -> str:
        return f"\n" \
               f"Settings: \n" \
               f"\tcuda={self.cuda}\n" \
               f"\tenhance_image={self.enhance_image}\n" \
               f"\tlanguage={self.language}\n" \
               f"\ttheme={self.theme}"


class PathSettings:
    """
    a class that stores paths for temporary files
    it manages paths for input/output files (and any temporary file in between)
    for paths for program configurations, refer to program settings
    """

    def __init__(self, output_folder: Path, source: Path = None):
        self.output_folder_parent = output_folder
        self.source: Path = source

    def _can_return(self) -> bool:
        return self.source is not None

    def set_input_file(self, source: Path):
        self.source = source

    def get_output_base_directory(self) -> Path:
        if self._can_return():
            return self.output_folder_parent.joinpath(self.source.name[:-len(self.source.suffix)])

    def get_unmarked_image_directory(self) -> Path:
        return self.get_output_base_directory().joinpath('cropped')

    def get_marked_image_directory(self) -> Path:
        return self.get_output_base_directory().joinpath('image_out')

    def get_marked_text_directory(self) -> Path:
        return self.get_output_base_directory().joinpath('text_out')


class WorkerSJob(QtCore.QThread):
    """
    Worker thread for image processing
    """

    job_abort_signal = pyqtSignal(Exception)
    job_complete_signal = pyqtSignal()
    job_progress_update_signal = pyqtSignal(str, int)
    job_log_signal = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        pass

    def report_progress(self, message: str, progress: int):
        self.job_progress_update_signal.emit(message, progress)

    def report_progress_progress_int(self, progress):
        self.job_progress_update_signal.emit(None, progress)

    def report_progress_progress_str(self, message):
        self.job_progress_update_signal.emit(message, None)

    def report_to_log(self, message):
        self.job_log_signal.emit(message)


