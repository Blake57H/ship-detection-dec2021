<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>MainWindow</source>
        <translation>程序</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <location filename="main_window.ui" line="0"/>
        <source>Select Source</source>
        <translation>选择数据</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Select XML</source>
        <translation>选择XML</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Start</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Activity Log</source>
        <translation>程序日志</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Select &quot;source&quot; to continue</source>
        <translation>请选择数据</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Source:</source>
        <translation>数据源：</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Image Info</source>
        <translation>图像信息</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>File Path</source>
        <translation>图像文件</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Select a file to continue</source>
        <translation>请选择文件</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Resolution</source>
        <translation>解析度</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Corner Coordinate</source>
        <translation>角落坐标</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Top Left</source>
        <translation>左上</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Top Right</source>
        <translation>右上</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Bottom Left</source>
        <translation>左下</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Bottom Right</source>
        <translation>右下</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Configurations</source>
        <translation>配置</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Enable CUDA Acceleration</source>
        <translation>启用CUDA加速</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Enhance Source Image</source>
        <translation>图片色彩增强</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Process Result</source>
        <translation>处理结果</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Result will be displayed here</source>
        <translation>处理结果在此展示</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>GroupBox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Save Sheet Data As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Export Whole Image As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <location filename="main_window.ui" line="0"/>
        <source>Preference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Acticity Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Start Processing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Stop Processing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window.ui" line="0"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
