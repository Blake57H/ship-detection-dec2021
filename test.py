import glob
import os
import re
import traceback

import sys
from datetime import datetime, timedelta

import imageio
import numpy
import tifffile
import pathlib
import time
import png
import math
import xml.etree.ElementTree as et
from datetime import datetime

from tifffile.tifffile import TiffFile

import controller_main_window
import general_functions

import image_manipulate
from core.test import detect
from image_manipulate import tiff2png, save_as, SaveFormat, ImageCropper, format_shape_structure, transpose, \
    GeoTransformer, get_cropped, ImageMerger

from cv2 import cv2

from osgeo import gdal, osr, ogr
from numpy import array

import pystretch


def loop_print(subject):
    for item in subject:
        print(item)


def rpc_calculation(p, l, h, rpc_tag_value):
    if rpc_tag_value is None:
        global tif_file
        file = tifffile.TiffFile(tif_file.__str__())
        rpc_tag_value = file.pages[0].tags['RPCCoefficientTag'].value
    if len(rpc_tag_value) != 92:
        raise ValueError(f"Incorrect tag length: {len(rpc_tag_value)}, expected 92")

    print((rpc_tag_value[0], rpc_tag_value[1]))

    p = (p - rpc_tag_value[4]) / rpc_tag_value[9]
    l = (l - rpc_tag_value[5]) / rpc_tag_value[10]
    h = (l - rpc_tag_value[6]) / rpc_tag_value[11]

    def _sum(p, l, h, coef):
        _result = coef[0] + coef[1] * l + coef[2] * p + coef[3] * h + \
                  coef[4] * l * p + coef[5] * l * h + coef[6] * p * h + coef[7] * (l ** 2) + \
                  coef[8] * (p ** 2) + coef[9] * (h ** 2) + coef[10] * p * l * h + coef[11] * (l ** 3) + \
                  coef[12] * l * (p ** 2) + coef[13] * l * (h ** 2) + coef[14] * (l ** 2) * p + coef[15] * (p ** 3) + \
                  coef[16] * p * (h ** 2) + coef[17] * (l ** 2) * h + coef[18] * (p ** 2) * h + coef[19] * (h ** 3)
        return _result

    rn = _sum(p, l, h, rpc_tag_value[12:32]) / _sum(p, l, h, rpc_tag_value[32:52])
    cn = _sum(p, l, h, rpc_tag_value[52:72]) / _sum(p, l, h, rpc_tag_value[72:92])
    r = rn * rpc_tag_value[7] + rpc_tag_value[2]
    c = cn * rpc_tag_value[8] + rpc_tag_value[3]
    return r, c


def tif_convert_pypng():
    tif_file = pathlib.Path(
        "source/extracted/GF1_PMS2_E102.8_N24.9_20200827_L1A0005017557/GF1_PMS2_E102.8_N24.9_20200827_L1A0005017557-PAN2.tiff")

    pages = tifffile.TiffFile(tif_file.__str__()).pages
    vertical = 3000
    horizontal = 3500
    print(len(pages))
    for page in pages:
        print(page.shape)
        print(page.dtype)
        test = page.asarray('memmap')
        # print(test)
        print(f"{len(test)}, {len(test[0])}, {len(test[0][0])}")
        print(test.shape)
        # time.sleep(10)
        save = test[vertical:vertical + 500, horizontal:horizontal + 500, :]
        # print(save)
        print(save.shape)
        tifffile.imwrite('beta-output/test.tiff', save)
        # save_array = numpy.array(save)
        # print(type(save_array))
        save_array = []
        max_val = numpy.max(test)
        scale = 255 / 1023
        print(max_val)
        for row in save:
            r = []
            for column in row:
                for element in column:
                    r.append(element * scale)
            save_array.append(r)
        save_array = numpy.array(save_array, dtype='uint8')
        print(save_array.shape)
        png_image = open('beta-output/test.png', 'wb')
        png.Writer(width=500, height=500, greyscale=False, alpha=True, bitdepth=8).write(png_image, save_array)
        png_image.close()

        '''
        for row in save:
            r = []
            for column in row:
                for element in column:
                    r.append(element)
            save_array.append(r)
        save_array = numpy.array(save_array)
        print(save_array.shape)
        png_image = open('beta-output/test.png', 'wb')
        png.Writer(width=500, height=500, greyscale=False, alpha=True, bitdepth=8).write(png_image, save_array)
        png_image.close()
        '''


def tif_convert_imageio():
    png_file = pathlib.Path('beta-output/test.png')
    tif_file = pathlib.Path(
        "source/extracted/GF2_PMS1_E122.1_N30.3_20190908_L1A0004235285/GF2_PMS1_E122.1_N30.3_20190908_L1A0004235285-MSS1.tiff")
    file = imageio.imread(tif_file.__str__())
    imageio.imwrite(png_file, file)


def tif_convert_pypng_ver2():
    tif_file = pathlib.Path(
        "source/extracted/GF2_PMS1_E122.1_N30.3_20190908_L1A0004235285/GF2_PMS1_E122.1_N30.3_20190908_L1A0004235285-MSS1.tiff")

    pages = tifffile.TiffFile(tif_file.__str__()).pages
    vertical = 3000
    horizontal = 3500
    print(len(pages))
    for page in pages:
        print(page.shape)
        print(page.dtype)
        test = page.asarray('memmap')
        # print(test)
        max_val = numpy.max(test)
        print(max_val)
        print(f"{len(test)}, {len(test[0])}, {len(test[0][0])}")
        print(test.shape)
        # time.sleep(10)
        save = test[vertical:vertical + 500, horizontal:horizontal + 500, :]
        # print(save)
        print(save.shape)
        tifffile.imwrite('beta-output/test.tiff', save)
        # save_array = numpy.array(save)
        # print(type(save_array))
        save_array = []
        for row in save:
            r = []
            for column in row:
                count = 0
                for element in column:
                    if count == 3:
                        continue
                    r.append(element / 4)
                    count += 1
            save_array.append(r)
        save_array = numpy.array(save_array, dtype='uint8')
        print(save_array.shape)
        png_image = open('beta-output/test.png', 'wb')
        png.from_array(save_array, 'RGB;8').write(png_image)
        png_image.close()


def tif_convert_opencv():
    global tif_file, png_file
    im = cv2.imread(tif_file.__str__(), cv2.IMREAD_COLOR)
    res = im.astype(numpy.uint16) / 4
    res = numpy.array(res, dtype='uint8')
    cv2.imwrite(png_file.__str__(), res)


def read_geo_location():
    global tif_file

    def get_geo(x_pixel, y_pixel, _geo_transform):
        x = _geo_transform[0] + x_pixel * _geo_transform[1] + y_pixel * _geo_transform[2]
        y = _geo_transform[3] + x_pixel * _geo_transform[4] + y_pixel * _geo_transform[5]
        return x, y

    dataset: gdal.Dataset = gdal.Open(tif_file.__str__())

    wkt_srs = dataset.GetProjection()
    geo_transform = dataset.GetGeoTransform()
    print(geo_transform)
    print(dataset.GetGCPs())

    src_srs = osr.SpatialReference()
    src_srs.ImportFromWkt(wkt_srs)

    tar_srs = osr.SpatialReference()
    tar_srs.ImportFromEPSG(4326)

    src_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    tar_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

    ct = osr.CoordinateTransformation(src_srs, tar_srs)

    ulx, uly = gdal.ApplyGeoTransform(geo_transform, 0, 0)
    print(ct.TransformPoint(ulx, uly))

    print((dataset.RasterXSize, dataset.RasterYSize))
    pages = tifffile.TiffFile(tif_file.__str__()).pages
    for _page in pages:
        height_value = _page.tags['ImageLength'].value
        width_value = _page.tags['ImageWidth'].value
        print(get_geo(0, 0, geo_transform))
        print(get_geo(0, height_value, geo_transform))
        print(get_geo(width_value, 0, geo_transform))
        print(get_geo(width_value, height_value, geo_transform))
        print('------------')


def gdal_read_tif_coordinate():
    tif_file = pathlib.Path(
        r"source/2021年3月25日舟山市.tif"
        # r"source\extracted\GF1B_PMS_E122.0_N30.2_20201216_L1A1227909135\GF1B_PMS_E122.0_N30.2_20201216_L1A1227909135-MUX.tiff"
    )
    ds: gdal.Dataset = gdal.Open(tif_file.__str__())
    print(f"info: {gdal.Info(ds, options=gdal.InfoOptions(format='json'))}")

    proj = osr.SpatialReference(wkt=ds.GetProjection())

    source_projection_id = proj.GetAttrValue("AUTHORITY", 1)
    print(source_projection_id)
    target_projection_id = 4326
    proj_source = osr.SpatialReference()
    proj_source.ImportFromEPSG(int(source_projection_id))
    proj_target = osr.SpatialReference()
    proj_target.ImportFromEPSG(target_projection_id)
    proj_target.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    Point = ogr.Geometry(ogr.wkbPoint)
    Point.AddPoint(394641.674, 3336696.276)  # use your coordinates here
    Point.AssignSpatialReference(proj)  # tell the point what coordinates it's in
    Point.TransformTo(proj_target)  # project it to the out spatial reference
    print('{0},{1}'.format(Point.GetX(), Point.GetY()))  # output projected X and Y coordinates


def read_tif_data():
    tif_file = pathlib.Path(
        "source/高分一号影像(png+tif)/20190401.tif"
        # r"source/2021年3月25日舟山市.tif"
        # r"source\extracted\GF1B_PMS_E122.0_N30.2_20201216_L1A1227909135\GF1B_PMS_E122.0_N30.2_20201216_L1A1227909135-MUX.tiff"
    )

    print("\n=====GDAL=====")
    ds: gdal.Dataset = gdal.Open(tif_file.__str__())
    # dataset = rasterio.open(tif_file.__str__())
    print(f"size: {ds.RasterXSize}x{ds.RasterYSize}")
    print(f"projection: {ds.GetProjection()}")
    print(f"gcp projection: {ds.GetGCPProjection()}")
    print(f"transform: {ds.GetGeoTransform()}")
    print(f"min={numpy.min(ds.ReadAsArray())}")
    # print(f"info: {gdal.Info(ds, options=gdal.InfoOptions(format='json'))}")
    return

    tif = tifffile.TiffFile(tif_file.__str__())
    print("\n=====TIFFFILE TAGS=====")
    for page in tif.pages:
        for tag in page.tags:
            tag_name, tag_value = tag.name, tag.value
            if tag_name in ['TileOffsets', 'TileByteCounts', 'StripOffsets', 'StripByteCounts']:
                continue
            elif tag_name in ["RPCCoefficientTag"]:
                print(len(tag_value))
            print(f"name={tag_name}, value={tag_value}")
        print("================")
        print(
            # f"{page.tags['CellWidth']} \n"
            # f"{page.tags['CellLength']} \n"
            f"{page.tags.__str__()} \n"
            # f"{page.tags['ResolutionUnit']} \n"
        )
        print("================")
        print(page.imagedepth)
        print(page.samplesperpixel)
        print(page.shape)

    print("\n====other====")
    print((tif.pages[0].tags.valueof("ImageLength")))

    return
    print("\n=====RASTER IO=====")
    ds: rasterio.DatasetReader = rasterio.open(tif_file.__str__())
    print((ds.width, ds.height))
    print(ds.bounds)
    print(ds.transform)


def subject1_convert_whole_image():
    print(f'{datetime.now()} start -> read tif')
    image_page = tifffile.TiffFile(tif_file).pages[0]
    image = image_page.asarray('memmap')
    order = format_shape_structure(image_page)
    if (0, 1, 2) != order:
        print(order)
        image = transpose(image, order)
        print(image.shape)
    print(f'{datetime.now()} read tif -> converting bit depth')
    # image = tiff2png(image)
    image = image_manipulate.drop_alpha(image)
    image = image_manipulate.uint16_to_uint8_ver2(image)
    # print(image.shape)
    filename = "beta-output/whole.png"
    print(f'{datetime.now()} read tif -> converting bit depth -> saving image')
    save_as(_array=image, _type=SaveFormat.PNG, filepath=pathlib.Path(filename))
    print(f'{datetime.now()} read tif -> converting bit depth -> saving image -> cropping')
    cropper = ImageCropper(256, 256,
                           pathlib.Path('beta-output').joinpath(tif_file.name[:-len(tif_file.suffix)]),
                           SaveFormat.JPG)
    cropper.execute(tif_page=tifffile.TiffFile(filename).pages[0], crop_only=True)
    print(f"{datetime.now()} complete")
    time.sleep(10)


def subject1_convert_whole_image_ver2():
    string = 'start -> read tif '
    print(f'{datetime.now()} {string}')
    global tif_file
    image_page = tifffile.TiffFile(tif_file).pages[0]
    output = pathlib.Path('temp')
    ''''''
    height_value = image_page.tags['ImageLength'].value
    width_value = image_page.tags['ImageWidth'].value
    ''''''
    print(f"{datetime.now()} {string}<- image size is {width_value}x{height_value}")
    clean_temporary_folder(output.__str__())

    # 7000 by 7000 is my laptop's ram limitation
    the_switch = height_value > 7000 or width_value > 7000

    if the_switch:
        cropper = ImageCropper(math.ceil(width_value / 2), math.ceil(height_value / 2),
                               output,
                               SaveFormat.TIFF)
        string += '-> split to 4 pics and enhance '
        ''''''
    else:
        cropper = ImageCropper(width_value, height_value,
                               output,
                               SaveFormat.TIFF)
        string += '-> enhancing '

    print(f'{datetime.now()} {string}')
    cropper.execute(image_page, crop_only=False)
    files = output.glob('*.tiff')
    output = pathlib.Path('beta-output').joinpath(tif_file.name[:-len(tif_file.suffix)])
    cropper = ImageCropper(256, 256, None, SaveFormat.JPG)
    counter = 0
    for file in files:
        counter += 1

        if the_switch:
            some_string = counter.__str__()
            cropper.set_output_folder(output.joinpath(some_string))
            cropper.set_filename_postfix(some_string)
        else:
            cropper.set_output_folder(output)

        print(f'{datetime.now()} {string}-> cutting pic {counter}')
        cropper.execute(TiffFile(file).pages[0], crop_only=True, sub_directory_count=(0 if the_switch else 4))
    string += '-> cut 4 pic -> complete'
    print(f'{datetime.now()} {string}')
    time.sleep(10)


def subject2_geo_transform_test():
    gt = GeoTransformer(tif_file)
    print(gt)


def read_xml():
    tif_file = pathlib.Path("source/extracted/GF1_PMS1_E102.5_N25.0_20200827_L1A0005017799/GF1_PMS1_E102.5_N25.0_20200827_L1A0005017799-MSS1.tiff")
    name = tif_file.name
    suffix = tif_file.suffix
    path = tif_file.parent
    print(tif_file.parts)
    print(name)
    print(path)
    print(suffix)
    xml_filename = name[:-len(suffix) + 1] + 'xml'
    xml_full_path = path.joinpath(xml_filename)
    print(xml_full_path.exists())
    xml_data = et.parse(xml_full_path.__str__()).getroot()
    print(xml_data.tag)
    print(xml_data.attrib)
    # print(xml_data.text)
    a = (xml_data.find('TopLeftLongitude').text, xml_data.findtext('TopLeftLongitude'))
    # a = float('')
    print(xml_data.find('TopLeftMapX').text)
    print(a)
    print(None in a)
    test_name = pathlib.Path("someFile")
    print(f"empty suffix = '{test_name.suffix}' from '{test_name.name}'")
    test_name = pathlib.Path(test_name.parent).joinpath(f"{test_name.name}.jpg")
    print(f"new suffix = '{test_name.suffix}' from '{test_name.name}'")


def test1():
    get_cropped()


def array_replace():
    a = numpy.asarray([a for a in range(40)])
    print(a)
    a = numpy.reshape(a, (4, 5, 2))
    a = numpy.zeros((4, 5, 2), dtype='uint8')
    print(a)
    b = numpy.asarray([100 for a in range(8)])
    b = numpy.reshape(b, (2, 2, 2))
    print(b)
    a[0:2, 1:3, :] = b
    print(a)


def regex_test():
    a = "r0_c10_1.jpg"
    rule = r'r(\d+)_c(\d+).'
    match = re.search(rule, a)
    print(match)
    l = re.compile(r'\d+').findall(match.group())
    print(l)
    print(int(l[0]))


def image_tile_merge_test():
    input_dir = pathlib.Path('beta-output/.1')
    output_file = 'beta-output/test.jpg'
    gtr = GeoTransformer(tif_file)
    merger = ImageMerger(gtr.get_width(), gtr.get_height(), 256)
    for image in input_dir.glob('*.jpg'):
        merger.add_image_tile(image)
    merger.save(output_file)
    print("completed, check ram usage")
    time.sleep(60)


def detect_test():
    input = 'beta-output/.1'
    detect(
        input,
        "yolo_m",
        {
            "classes": ["ship"],
            "confidence": 0.5,
            "nms_iou": 0.3,
            "cuda": False
        }
    )


def result_data_load():
    # input = pathlib.Path("beta-output/.1/txt_results/r11_c10.txt")
    # with open(input.__str__(), 'r') as fs:
    #     while True:
    #         line = fs.readline()
    #         a = line.strip().split(' ')
    #         print(a)
    #         if line is None or line == '':
    #             break
    # return
    #
    input = pathlib.Path("beta-output/.1/")
    a = controller_main_window
    a.geo_transform_info = a.GeoTransformer(tif_file)
    a.result_set = a.ResultSet(input)
    for item in a.result_set.get_all():
        print(item)


def error_test():
    try:
        # assert False, "Raise a assertion exception"
        b = 1 / 0
    except Exception as ex:
        print(ex)
        print("=====")
        print(ex.args)
        print("=====")
        print(repr(ex))
        print("=====")
        print(sys.exc_info())

        (_type, value, _traceback) = sys.exc_info()
        print("=====")
        print(_type)
        print("=====")
        print(value)
        print("=====")
        print(_traceback)
        print("=====")
        print(traceback.format_exception(_type, value, _traceback))


def lambda_test():
    def run_func(func, *args):
        func(*args)

    run_func(lambda a: print(a))


def datetime_test():
    now = datetime.now().strftime("%y")


def save_language():
    general_functions.LanguagePack().save_translation()


def crop_image():
    tif_file = pathlib.Path(
        r"source/2021年3月25日舟山市.tif"
        # r"source\extracted\GF1B_PMS_E122.0_N30.2_20201216_L1A1227909135\GF1B_PMS_E122.0_N30.2_20201216_L1A1227909135-MUX.tiff"
    )


def scan_folder():
    theme_dir = pathlib.Path("theme")
    items = theme_dir.glob("*")
    for item in items:
        print(item)
        print(item.name)
        print(item.stem)



png_file = pathlib.Path('beta-output/test.png')

# tif_convert_imageio()
# tif_convert_pypng_ver2()
# tif_convert_opencv()
# read_geo_location()
# gdal_read_tif_coordinate()
# read_tif_data()
read_xml()
# subject1_convert_whole_image()
# subject1_convert_whole_image_ver2()
# subject2_geo_transform_test()
# print(rpc_calculation(22.686, 105.073, 0, None))
# array_replace()
# regex_test()
# image_tile_merge_test()
# detect_test()
# result_data_load()
# error_test()
# save_language()
# scan_folder()

exit(0)

dir = pathlib.Path('input/')
loop_print(dir.glob('*.tif'))
loop_print(glob.glob('input/*.tif'))
exit(0)

print(len(tif.pages))


class BClass:
    def __init__(self):
        pass


b = BClass()

a = "abdjd"
print(type(b))
assert isinstance(b, BClass)

image = tif.pages[0]

data_body: array = image.asarray()

print(data_body)
print(len(data_body.shape))

print(image.shape)

print(image.dtype)

with open('./output/size test.txt') as result:
    text = result.readlines()
    lines = len(text)
    word = 0
    for line in text:
        word += len(line.split())
    print(f"{lines}, {word}")
