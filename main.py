import platform
import tkinter as tk

import sys


class VerInfo:
    def __init__(self, v1, v2, v3=None):
        self.v1 = v1
        self.v2 = v2
        self.v3 = v3

    def __str__(self):
        string = '{}.{}'.format(self.v1, self.v2)
        if self.v3 is not None:
            return '{}.{}'.format(string, self.v3)
        return string


def show_error(title, message, exception):
    root = tk.Tk()
    root.geometry("400x200")
    root.title(title)
    label = tk.Label(root, text=message)
    label.pack(side="top", fill="both", expand=False, padx=20, pady=20)
    button = tk.Button(root, text="OK", command=lambda: root.destroy())
    button.pack(side="bottom", fill="none", expand=True)
    root.mainloop()
    if exception is not None:
        raise exception(title + "\n" + message)
    else:
        print(title + "\n" + message)


if __name__ == '__main__':
    min = VerInfo(3, 7)
    rec = VerInfo(3, 9, 9)
    # show_error(
    #     "Are you using python %d?" % min.v1,
    #     "This script is for Python {}, run at your own risk.\n"
    #     "Minimum: {}\n" 
    #     "Suggested: {}\n" 
    #     "You have: {}".format(min.v1, min, rec, platform.python_version()),
    #     None
    # )
    # exit(0)
    ver_info = sys.version_info
    if ver_info[0] <= min.v1:
        if ver_info[1] < min.v2 or ver_info[0] < min.v1:
            show_error(
                "Incompatible python interpreter",
                "Minimum: {}\n"
                "Suggested: {}\n"
                "You have: {}".format(min, rec, platform.python_version()),
                OSError
            )
        elif ver_info[1] < rec.v2:
            exec('from __future__ import annotations')
        else:
            pass
    else:
        show_error(
            "Are you using python %d?" % min.v1,
            "This script is for Python {}, run at your own risk.\n"
            "Minimum: {}\n"
            "Suggested: {}\n"
            "You have: {}".format(min.v1, min, rec, platform.python_version()),
            None
        )
    exec('import controller_main_window')
    exec('controller_main_window.init_exec()')
else:
    print(":)")
