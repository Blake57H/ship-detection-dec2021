import pathlib

from test_window_background_view import Ui_MainWindow
from PyQt6 import QtCore, QtGui, QtWidgets

image_path = pathlib.Path("theme/satellite-sample/background.jpg")
print(image_path.exists())

stylesheet = \
    '#MainWindow {' \
    'background-image: url("theme/satellite-sample/background.jpg");' \
    'background-repeat: no-repeat;' \
    'background-position: center;' \
    "} " \
    '#tab, #tab_2 {' \
    'background-image: url("theme/satellite-sample/sticker.png");' \
    'background-repeat: no-repeat;' \
    'background-position: right bottom;' \
    "} "
print(stylesheet)

if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    # app.setStyle(stylesheet)
    window = QtWidgets.QMainWindow()
    window.setWindowIcon(QtGui.QIcon("F:\Website\speedtest-master\.logo\icon_huge.png"))
    # window.setStyleSheet(stylesheet)
    # window.repaint()

    # image = QtGui.QImage(image_path.__str__())
    # image = image.scaled()
    # palette = QtGui.QPalette()
    # palette.setBrush(QtGui.QPalette.ColorRole.Window, QtGui.QBrush(image))
    # window.setPalette(palette)

    ui = Ui_MainWindow()
    ui.setupUi(window)
    # ui.tabWidget.setPalette(QtGui.QPalette(QtGui.QColor(128, 0, 0, 128),QtGui.QColor(128, 0, 0, 128),QtGui.QColor(128, 0, 0, 128),QtGui.QColor(128, 0, 0, 128),QtGui.QColor(128, 0, 0, 128),QtGui.QColor(128, 0, 0, 128),QtGui.QColor(128, 0, 0, 128),QtGui.QColor(128, 0, 0, 128),QtGui.QColor(128, 0, 0, 128)))
    # plat = ui.tabWidget.palette()
    # plat.setBrush(QtGui.QPalette.ColorRole.Base, QtGui.QColor(128, 0, 0, 128))
    # ui.centralwidget.setPalette(plat)
    # ui.tab.setStyleSheet("background: rgba(128, 0, 0, 0.5);")
    # ui.centralwidget.setAttribute(QtCore.Qt.WidgetAttribute.WA_TranslucentBackground, True)
    # ui.centralwidget.setStyleSheet("background: transparent;")
    window.show()
    app.setStyleSheet(stylesheet)  # <---
    sys.exit(app.exec())
