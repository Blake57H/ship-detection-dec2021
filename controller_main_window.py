import datetime
import json

import gc
import os
import traceback
from pathlib import Path
from typing import List, Optional

import sys
import tifffile
from PyQt6 import QtCore, QtWidgets, QtGui
from PyQt6.QtCore import pyqtSignal

import view_main_window
from execute_model import DetectModel
from general_functions import show_warning, LanguagePack, log_function, LogFileName, clean_temporary_folder
from image_manipulate import GeoTransformer, ImageCropperReversed, SaveFormat, ImageMerger
from controller_activity_log_dialog import ActivityLogDialog
from model_main_window import ProgramSetting, PathSettings, WorkerSJob


def exception_handler(etype, value, tb):
    """
    Override default exception handler, to fit in a message box....
    """
    # print("exception_handler called")  # DEBUG
    message_lines = traceback.format_exception(etype, value, tb)
    reason_of_error = message_lines[len(message_lines) - 1].strip()
    error_msg = ''.join(message_lines)
    # error_msg = etype.__str__() + ";" + value.__str__() + ";" + tb.__str__()
    log_and_print(error_msg)
    # todo: show a messagebox (done) and reset program
    show_message_box(lp.error_popup_title, lp.error_popup_body, reason_of_error, error_msg,
                     QtWidgets.QMessageBox.Icon.Critical,
                     QtWidgets.QMessageBox.StandardButton.Reset | QtWidgets.QMessageBox.StandardButton.Abort | QtWidgets.QMessageBox.StandardButton.Ignore,
                     on_error_msg_box_button_clicked)


def raise_exception_in_main_thread(exception: Exception):
    """
    This function should be called only from main thread
    """
    raise exception


def show_message_box(title: str, text: str, informative_text: str = None, detailed_text: str = None, icon=None,
                     button_set=QtWidgets.QMessageBox.StandardButton.Ok, button_click_event: callable = None):
    """
    a qmessagebox builder? pass all parameters and, message box off you go
    """
    msg_box = QtWidgets.QMessageBox()
    msg_box.setWindowTitle(title)
    msg_box.setText(text)
    if informative_text is not None:
        msg_box.setInformativeText(informative_text)
    if icon is not None:
        msg_box.setIcon(icon)
    msg_box.setStandardButtons(button_set)
    if detailed_text is not None:
        msg_box.setDetailedText(detailed_text)
    if button_click_event is not None:
        msg_box.buttonClicked.connect(button_click_event)
    msg_box.exec()


def on_error_msg_box_button_clicked(i: QtWidgets.QPushButton):
    """
    yes?
    :param i: the button got clicked
    :return:
    """
    # print(i == QtWidgets.QMessageBox.StandardButton.Reset)  # debug
    # print(i.text())  # debug
    if i.text() == 'Abort':
        _set_executing_status(False)
        _app.quit()
    elif i.text() == 'Reset':
        reset_program(True)
    elif i.text() == 'Ignore':
        pass  # ignore button exists but does nothing


def on_confirm_exit_msg_box_button_clicked(i: QtWidgets.QPushButton):
    """
    yes?
    :param i: the button got clicked
    :return:
    """
    # print(i == QtWidgets.QMessageBox.StandardButton.Reset)  # debug
    # print(i.text())  # debug
    if 'Yes' in i.text():
        _set_executing_status(False)
        log_and_print(lp.exit_program)
        sys.exit(0)
    elif 'No' in i.text():
        pass


class MainWindow(QtWidgets.QMainWindow):
    """
    subclass main window to fit in a close event
    """

    # noinspection PyPep8Naming
    def closeEvent(self, QCloseEvent):
        log_and_print(lp.exit_program)
        if can_exit():
            QCloseEvent.accept()
        else:
            show_message_box(lp.exit_program_confirm_title, lp.exit_program_confirm_text, None, None,
                             QtWidgets.QMessageBox.Icon.Question,
                             QtWidgets.QMessageBox.StandardButton.Yes | QtWidgets.QMessageBox.StandardButton.No,
                             on_confirm_exit_msg_box_button_clicked)
            log_and_print(lp.exit_program_canceled)
            QCloseEvent.ignore()


class ResultSet(QtCore.QAbstractTableModel):
    """
    consider it a table. Or consider a list of rows, and each row got 3 columns(ResultItem.get_header).
    Usually loading results wouldn't take long.
    Anyway, there is a finish signal, just in case someone wants to run it in a thread.
    """
    finished_signal = QtCore.pyqtSignal()

    # test_signal = QtCore.pyqtSignal(str, int)

    class ResultItem:
        longitude: float
        latitude: float
        confidence: float
        marked_image: str

        def __init__(self, longitude: float, latitude: float, confidence: float, marked_image: str):
            self.longitude = longitude
            self.latitude = latitude
            self.confidence = confidence
            self.marked_image = marked_image

        def __str__(self):
            return f"longitude={self.longitude}; latitude={self.latitude}; confidence={self.confidence}; image={self.marked_image}"

        # def index(self, i):
        #     if i == 0:
        #         return self.index
        #     elif i == 1:
        #         return self.longitude
        #     elif i == 2:
        #         return self.latitude
        #     else:
        #         return QtCore.QVariant()

        @staticmethod
        def get_header():
            return ['index', 'longitude', 'latitude']

        @staticmethod
        def get_csv_header(index: bool = True):
            string = ''
            header = ResultSet.ResultItem.get_header()
            header_length = len(header)
            for count in range(header_length):
                if not index and count == 0:
                    continue
                string += header[count]
                if count != header_length - 1:
                    string += ','
            return string

        def get_csv_body(self, index: int = None):
            string = f"{self.longitude},{self.latitude}"
            if index is not None:
                string = f"{index},{string}"
            return string

    def __init__(self, path_settings_base: PathSettings):
        super().__init__()
        self.unmarked_image_segments = path_settings_base.get_unmarked_image_directory()
        self.marked_image_segments = path_settings_base.get_marked_image_directory()
        self.marked_text = path_settings_base.get_marked_text_directory()
        self.result_list: List[ResultSet.ResultItem] = list()
        return

    def setup_list(self):
        """
        read all text results then parse all them into a list
        """

        # validating
        assert self.unmarked_image_segments.exists() and self.unmarked_image_segments.is_dir(), \
            "cannot find unmarked results"
        assert self.marked_image_segments.exists() and self.marked_image_segments.is_dir(), \
            "cannot find marked results"
        assert self.unmarked_image_segments.exists() and self.unmarked_image_segments.is_dir(), \
            "cannot find text results"
        assert geo_transform_info is not None, "Geo info data hasn't initialized. Setup 'GeoTransformer' first."

        # load data
        for item in self.marked_text.glob('*.txt'):
            marked_image = self.marked_image_segments.joinpath(item.name[:-len(item.suffix)] + SaveFormat.JPG).__str__()
            with open(item, 'r') as stream:
                while True:
                    line = stream.readline()

                    if line is None or line.strip() == '':
                        break

                    values = line.split()
                    if len(values) != 5:
                        show_warning("Unrecognized result text. Has it updated?")
                        continue
                    x = int((int(values[0]) + int(values[2])) / 2)
                    y = int((int(values[1]) + int(values[3])) / 2)
                    x_geo, y_geo = geo_transform_info.pixel_to_decimal_geo_with_filename(Path(item).name, x, y, 256)
                    self.result_list.append(self.ResultItem(x_geo, y_geo, float(values[4]), marked_image))
        self.finished_signal.emit()
        # self.test_signal.emit("hi", 100)

    def get_all(self):
        return self.result_list

    def get(self, index: int) -> ResultItem:
        return self.result_list[index]

    def rowCount(self, parent=None, *args, **kwargs):
        """
        return the list's length as row count
        """
        return len(self.result_list)

    def columnCount(self, parent=None, *args, **kwargs):
        """
        return column count
        """
        return len(self.ResultItem.get_header())

    def headerData(self, column, Qt_Orientation, role=None):
        """
        i can't explain why do this but in order to get the model working, it has to be done
        since I only needs column headers, return when "Qt_Orientation" meets a certain condition
        and 'column' means the table view is requesting info regarding of that column
        """
        # print(column)
        # print(self.ResultItem.get_header())
        # print(Qt_Orientation)
        # print(role == QtCore.Qt.ItemDataRole.DisplayRole)
        if Qt_Orientation == QtCore.Qt.Orientation.Horizontal and role == QtCore.Qt.ItemDataRole.DisplayRole:
            return self.ResultItem.get_header()[column]
        # else:
        #     return QtCore.QVariant()

    def data(self, QModelIndex: QtCore.QModelIndex, role=None):
        """
        QModelIndex has row and column info, and "DisplayRole" means it wants to be in the table
        return a value when role="DisplayRole" and row & column fall into the list's range
        """

        if self.rowCount() == 0 or QModelIndex.row() >= self.rowCount() or \
                role != QtCore.Qt.ItemDataRole.DisplayRole or QModelIndex.column() >= self.columnCount():
            return QtCore.QVariant()
        else:
            # self.index(row = QModelIndex.row(), column=QModelIndex.column())
            if QModelIndex.column() == 0:
                return QModelIndex.row()
            if QModelIndex.column() == 1:
                return self.result_list[QModelIndex.row()].longitude.__str__()
            if QModelIndex.column() == 2:
                return self.result_list[QModelIndex.row()].latitude.__str__()


def on_table_selection_changed(selected: QtCore.QItemSelection, deselected: QtCore.QItemSelection):
    """
    when user click a cell, display the corresponding image
    and this is the listener for it
    """

    # if selected.indexes().__len__() != 0:
    #     print(selected.indexes()[0].row())
    # else:
    #     print(selected.indexes())
    # if deselected.indexes().__len__() != 0:
    #     print(deselected.indexes()[0].row())
    # else:
    #     print(deselected.indexes())
    # DEBUG

    # update image display if it is a new row
    if selected.indexes().__len__() != 0 or \
            deselected.indexes().__len__() != 0 and selected.indexes()[0].row() != deselected.indexes()[0].row():
        _main_ui.label_resultImageView.setPixmap(
            QtGui.QPixmap(result_set.get(selected.indexes()[0].row()).marked_image)
        )


# VARIABLES

_main_ui: view_main_window.Ui_MainWindow  # ui layout
# _source: Path  # unused
_mw: QtWidgets.QMainWindow  # custom main window class
_app: QtWidgets.QApplication  # application for ui
geo_transform_info: GeoTransformer  # geo info, if not none then program is initialized and can process image
# cropper: ImageCropperReversed
worker_thread: WorkerSJob  # you're a slave here (evil laughing)
worker_thread_running = False  # will be used in can_exit()
lp = LanguagePack()
log_filename = Path('logs').joinpath(f'{LogFileName().get_time_as_filename()}.txt')
# noinspection PyPep8Naming
result_set: ResultSet
path_settings = PathSettings(Path("beta-output-alpha/"))
program_settings = ProgramSetting()


class WorkerSJobSaveImage(WorkerSJob):
    def __init__(self, save_path: Path):
        super().__init__()
        self.save_path = save_path

    def run(self):
        try:
            global path_settings, geo_transform_info
            self.report_progress(lp.merge_image, 0)
            merger = ImageMerger(geo_transform_info.get_width(), geo_transform_info.get_height(), 256)
            image_list = [image for image in path_settings.get_marked_image_directory().iterdir()]
            list_length = len(image_list)
            count = 0
            for image in image_list:
                merger.add_image_tile(image)
                count += 1
                self.report_progress_progress_int(int(count * 100 / list_length))
            self.report_progress_progress_str(lp.save_image)
            merger.save(self.save_path)
            self.job_complete_signal.emit()
        except Exception as ex:
            self.job_abort_signal.emit(ex)


class WorkerSJobSaveSheet(WorkerSJob):
    def __init__(self, save_path: Path, data: List[ResultSet.ResultItem], header: List[str] = None):
        super().__init__()
        self.save_path = save_path
        self.data = data

    def run(self):
        try:
            with open(self.save_path.__str__(), 'w') as stream:
                stream.write(ResultSet.ResultItem.get_csv_header())
                stream.write('\n')

                size = len(self.data)
                for count in range(size):
                    stream.write(self.data[count].get_csv_body(count))
                    stream.write('\n')
                    self.report_progress_progress_int(int(count * 100 / size))
            self.job_complete_signal.emit()
        except Exception as ex:
            self.job_abort_signal.emit(ex)


class WorkerSJobProcessImage(WorkerSJob):
    def __init__(self, enable_cuda: bool, enhance_image: bool, enable_memmap: bool, clean_up_output_path: bool = True):
        super().__init__()
        self.enable_cuda = enable_cuda
        self.enhance_image = enhance_image
        self.enable_memmap = enable_memmap
        self.clean_up_output_path = clean_up_output_path

    def run(self):
        try:
            if self.clean_up_output_path:
                clean_temporary_folder(path_settings.output_folder_parent.__str__())

            # STEP ONE: CROP IMAGE INTO 256 BY 256 TILES, ENHANCE IMAGE COLOR IF NEEDED
            # _setup_cropper(path_settings.get_unmarked_image_directory())
            cropper = ImageCropperReversed(
                crop_width=256, crop_height=256,
                output_folder=path_settings.get_unmarked_image_directory(),
                output_format=SaveFormat.JPG,
                language_pack=lp,
                memmap=self.enable_memmap
            )
            cropper.update_progress_callback.connect(self.report_progress)
            cropper.execute(tifffile.TiffFile(path_settings.source.__str__()).pages[0],
                            crop_only=not self.enhance_image)
            # print('CROPPER COMPLETE')  # debug

            self.report_progress(lp.detecting_ships, 0)

            # STEP TWO: MAKE THE MODEL TO RECOGNIZE SHIPS IN THOSE IMAGE TILES
            dm = DetectModel(self.enable_cuda)
            dm.update_progress_signal.connect(self.report_progress_progress_int)
            # print("DETECT MODEL START")  # debug
            dm.run(
                input=path_settings.get_unmarked_image_directory().__str__(),
                image_output=path_settings.get_marked_image_directory().__str__(),
                text_output=path_settings.get_marked_text_directory().__str__()
            )

            self.report_progress_progress_str(lp.displaying_result)
            # STEP THREE: DISPLAY RESULT
            result_set = ResultSet(path_settings)
            result_set.setup_list()
            self.job_complete_signal.emit()

        except Exception as ex:
            # (_type, value, _traceback) = sys.exc_info()
            # sys.excepthook(_type, value, _traceback)
            self.job_abort_signal.emit(ex)


def log_and_print(message: str):
    """
    write message to log file and print it in terminal
    """
    print(message)
    log_function(log_filename, message)


def log_settings():
    log_and_print(program_settings.__str__())
    # log_and_print(f"CUDA Acceleration: {_main_ui.checkBox_enableCUDA.isChecked()}")
    # log_and_print(f"Enhance Image: {_main_ui.checkBox_enableImageEnhance.isChecked()}")


def clear_old_log(day: int = 7):
    clear_range = datetime.timedelta(days=day)
    log_path = Path('logs/')
    name_parser = LogFileName()
    current = name_parser.parse_filename(log_filename)
    for file in log_path.glob('*.txt'):
        subject = name_parser.parse_filename(file)
        if subject is None or current - subject > clear_range:
            file.unlink(True)


def display_log():
    global _mw, log_filename
    _mw.setEnabled(False)
    _mw.repaint()
    dialog = ActivityLogDialog(log_filename.__str__())
    dialog.finished.connect(lambda: _mw.setEnabled(True))
    dialog.show()
    dialog.exec()


def reset_program(got_hurt: bool = True):
    """
    got hurt = received an exception and need to reset program
    """
    global geo_transform_info, path_settings, lp, result_set, worker_thread
    if got_hurt:
        geo_transform_info = path_settings = result_set = worker_thread = None
        lp = LanguagePack()
        gc.collect()
        _update_info(True)

    # sync ui settings with backend program settings
    program_settings.from_json()
    # print(program_settings.get_log())
    if program_settings.is_language_available():
        trans = QtCore.QTranslator()
        trans.load(program_settings.get_display_language())
        _app.installTranslator(trans)
        _main_ui.retranslateUi(_mw)
        lp.load_translation(Path(program_settings.get_message_language()))

    _main_ui.checkBox_enableImageEnhance.setChecked(program_settings.get_enhance_image())
    _main_ui.checkBox_enableCUDA.setChecked(program_settings.get_cuda())
    _main_ui.checkBox_enableMemmap.setChecked(True)
    _main_ui.tabWidget.setCurrentIndex(0)

    # scan themes and add them to combobox
    theme_dir = program_settings.get_theme_dir()
    current_theme = program_settings.theme
    log_and_print(f"Theme in config file: {current_theme}")
    theme_count = 0
    if theme_dir.exists():
        themes = theme_dir.glob('*')
        for theme in themes:
            if theme.is_dir():
                theme_count += 1
                _main_ui.comboBox_config_theme.addItem(theme.name.__str__())
                if current_theme != '' and theme.name == current_theme:
                    _main_ui.comboBox_config_theme.setCurrentIndex(theme_count - 1)
        if theme_count == 0:
            _main_ui.comboBox_config_theme.addItem(lp.info_no_theme_available)
            _main_ui.comboBox_config_theme.setEnabled(False)
        elif _main_ui.comboBox_config_theme.currentIndex() == -1:
            _main_ui.comboBox_config_theme.setCurrentIndex(0)
    _apply_theme()
    _set_executing_status(False, False)
    _update_status_text(lp.program_ready)


def init_exec():
    """
    basically main()
    """
    global _main_ui, _mw, _app
    _main_ui = view_main_window.Ui_MainWindow()
    _app = QtWidgets.QApplication(sys.argv)
    _mw = MainWindow()
    _main_ui.setupUi(_mw)
    sys.excepthook = exception_handler

    _setup_listeners()
    _mw.show()
    reset_program(False)
    clear_old_log()
    return _app.exec()


def _open_file(filter: str):
    """
    when user select a source
    """
    global _main_ui
    dialog = QtWidgets.QFileDialog()
    if os.name != 'nt':
        # linux does not show dialog using dialog.getOpenFileName()
        a = dialog.getOpenFileName()
    else:
        a = dialog.getOpenFileName(filter='(*.tiff *.tif)')
    log_and_print(f"User selected a file: {a}")
    if a[0] != '' and Path(a[0]).exists():
        _load_image_info(Path(a[0]))
        _update_info()
        _set_executing_status(is_executing=False, is_initialized=True)
        return
    else:
        if not Path(a[0]).exists():
            # todo: translate
            show_message_box("File not found", f"The following file does not exist: {a[0]}", icon=QtWidgets.QMessageBox.Icon.Information)
        log_and_print("The selected file was ignored")


def _user_chooses_save_path(filter: str) -> Optional[Path]:
    """
    save file dialogue
    return a Path
    """
    global path_settings
    path = path_settings.source.parent
    name = path_settings.source.name[:-len(path_settings.source.suffix)] + filter

    dialog = QtWidgets.QFileDialog()
    if os.name == 'nt':
        a = dialog.getSaveFileName(filter=f'({filter})', directory=path.joinpath(name).absolute().__str__())
    else:
        a = dialog.getSaveFileName(directory=path.joinpath(name).absolute().__str__())
    log_and_print(f"User saving a file: {a}")
    if a[0] != '':
        path = Path(a[0])
        if path.is_dir():
            raise IsADirectoryError("Expecting a file, but received a directory")
        return path
    else:
        log_and_print("User don't want to save file.")
        return None


def _load_image_info(tiff_path: Path):
    """
    when user selected a valid source
    """
    global geo_transform_info
    log_and_print("loading tiff info")
    geo_transform_info = GeoTransformer(tiff_path)
    if geo_transform_info.init_message != "":
        log_and_print(geo_transform_info.init_message)
    log_and_print(geo_transform_info.__str__())
    path_settings.set_input_file(tiff_path)
    _update_status_text(lp.tif_image_loaded.format(tiff_path.name))


def can_exit() -> bool:
    """
    hmmm
    """
    # todo: check whether program is running, and exit if either not running or user abort execution
    global worker_thread_running
    return not worker_thread_running


def _setup_listeners():
    """
    I am so used to java that I forgot it is slots and signal
    """

    _main_ui.pushButton_source.clicked.connect(_open_file)
    _main_ui.pushButton_startStopProcessing.clicked.connect(worker_doing_its_job)
    _main_ui.pushButton_exportTable.clicked.connect(worker_save_data_csv)
    _main_ui.pushButton_exportImage.clicked.connect(worker_save_whole_image)
    _main_ui.pushButton_activityLog.clicked.connect(display_log)

    _main_ui.actionSelect_Source.triggered.connect(_open_file)
    _main_ui.actionExit.triggered.connect(_app.quit)
    _main_ui.actionStart_Processing.triggered.connect(worker_doing_its_job)
    _main_ui.actionPreference.triggered.connect(
        lambda: _main_ui.tabWidget.setCurrentIndex(1)
    )
    _main_ui.actionAbout.triggered.connect(
        lambda: show_message_box(
            lp.about_title,
            lp.about_body + "\n" + lp.about_license
        )
    )
    _main_ui.actionActicity_Log.triggered.connect(display_log)

    _main_ui.tableView_results.selectionChanged = on_table_selection_changed

    _main_ui.checkBox_enableCUDA.clicked.connect(program_settings.set_cuda)
    _main_ui.checkBox_enableImageEnhance.clicked.connect(program_settings.set_enhance_image)

    _main_ui.comboBox_config_theme.currentTextChanged.connect(_apply_theme)


def _on_result_return():
    """
    worker thread completed and main thread load result to gui
    """
    _set_executing_status(is_executing=False, is_completed=True)
    global result_set, path_settings
    # print(path_settings.get_output_base_directory())  # debug
    result_set = ResultSet(path_settings)
    result_set.setup_list()
    _main_ui.tableView_results.setModel(result_set)
    _main_ui.tabWidget.setCurrentIndex(2)
    result_size = len(result_set.get_all())
    if result_size == 0:
        _main_ui.label_resultImageView.setText(lp.no_result)
    elif result_size == 1:
        _main_ui.label_resultImageView.setText(lp.one_result.format(result_size))
    else:
        _main_ui.label_resultImageView.setText(lp.lots_of_result.format(result_size))
    _update_status_text(lp.execute_complete)


def worker_doing_its_job():
    """
    create(hire) a worker thread and run it (no salary)
    """
    _set_executing_status(True)
    assert geo_transform_info is not None, "GeoTransformer is required to execute"
    # assert cropper is not None  # moved cropper set up to worker's job

    # image enhance = 40%, image cropping = 10%, image recognize = 40%, result load = 10%
    progress = 0
    enable_cuda = _main_ui.checkBox_enableCUDA.isChecked()
    enhance_image = _main_ui.checkBox_enableImageEnhance.isChecked()
    enable_memmap = _main_ui.checkBox_enableMemmap.isChecked()
    clean_up_output_path = True
    global worker_thread
    worker_thread = WorkerSJobProcessImage(enable_cuda=enable_cuda, enhance_image=enhance_image,
                                           enable_memmap=enable_memmap, clean_up_output_path=clean_up_output_path)
    worker_thread.job_complete_signal.connect(
        # lambda: _complete_execution_cleanup(lp.execute_complete, _main_ui.tableView_results.setModel, result_set)
        # lambda result: _complete_execution_cleanup(lp.execute_complete, _display_result, result)
        _on_result_return
    )
    worker_thread.job_progress_update_signal.connect(_update_status)
    worker_thread.job_abort_signal.connect(raise_exception_in_main_thread)
    log_settings()
    worker_thread.start()


def worker_stopping_its_job():
    """
    not going to make a stop button
    """
    pass


def worker_cleanup_after_work(message: str, func: callable, *args):
    """
    more like a place for re-enabling widget, displaying status message, and execute a finalize function (callback)
    """
    _set_executing_status(is_executing=False, is_completed=True)
    if func is not None:
        func(*args)
    if message is not None and message != '':
        _update_status_text(message)


def worker_save_whole_image():
    if not path_settings.get_marked_image_directory().exists():
        show_message_box(lp.error_no_image_to_save_title, lp.error_no_image_to_save_body, icon=QtWidgets.QMessageBox.Icon.Critical)
    save_file = _user_chooses_save_path(f"{SaveFormat.JPG}")
    if save_file is None:
        return
    if save_file.suffix == '':
        save_file = Path(save_file.parent).joinpath(f"{save_file.name}.jpg")
    global worker_thread
    _set_executing_status(True)
    worker_thread = WorkerSJobSaveImage(save_file)
    worker_thread.job_progress_update_signal.connect(_update_status)
    worker_thread.job_abort_signal.connect(raise_exception_in_main_thread)
    worker_thread.job_complete_signal.connect(lambda: worker_cleanup_after_work(lp.execute_complete, None))
    worker_thread.start()


def worker_save_data_csv():
    global worker_thread, result_set
    if result_set is None:
        show_message_box("Can't find data to save", "Result seems to be empty.", icon=QtWidgets.QMessageBox.Icon.Critical)  # todo translate
    save_file = _user_chooses_save_path(f"{SaveFormat.CSV}")
    if save_file is None:
        return
    _set_executing_status(True)
    worker_thread = WorkerSJobSaveSheet(save_file, result_set.get_all())
    worker_thread.job_progress_update_signal.connect(_update_status)
    worker_thread.job_abort_signal.connect(raise_exception_in_main_thread)
    worker_thread.job_complete_signal.connect(lambda: worker_cleanup_after_work(lp.execute_complete, None))
    worker_thread.start()


# FUNCTIONS THAT UPDATES GUI: SHOW INFO, ENABLE & DISABLE CONTROL


def _update_info(reset=False):
    """
    update gui to reflect user selected source
    """
    if reset:
        log_and_print(lp.reset_geo_info)
        empty = ''
        _main_ui.label_source_content.setText(empty)
        _main_ui.label_imageInfo_filepath_content.setText(empty)
        _main_ui.label_imageInfo_resolution_content.setText(empty)
        _main_ui.label_imageInfo_cc_tl_content.setText(empty)
        _main_ui.label_imageInfo_cc_tr_content.setText(empty)
        _main_ui.label_imageInfo_cc_bl_content.setText(empty)
        _main_ui.label_imageInfo_cc_br_content.setText(empty)
    elif path_settings.source.exists() and path_settings.source.is_file():
        log_and_print(lp.read_geo_info)
        _main_ui.label_source_content.setText(path_settings.source.name)
        _main_ui.label_imageInfo_filepath_content.setText(path_settings.source.__str__())
        _main_ui.label_imageInfo_resolution_content.setText(
            geo_transform_info.get_resolution_str()
        )
        _main_ui.label_imageInfo_cc_tl_content.setText(geo_transform_info.tl.__str__())
        _main_ui.label_imageInfo_cc_tr_content.setText(geo_transform_info.tr.__str__())
        _main_ui.label_imageInfo_cc_bl_content.setText(geo_transform_info.bl.__str__())
        _main_ui.label_imageInfo_cc_br_content.setText(geo_transform_info.br.__str__())
        # log_and_print("updated tiff info")
    else:
        log_and_print(f"tiff file {path_settings.source} was ignored")


def _update_status_text(message: str):
    if message is not None and message != '':
        _main_ui.label_status.setText(message)
        log_and_print(message)


def _update_progress_bar(progress: int):
    if progress is not None:
        _main_ui.progressBar_status.setValue(progress)


def _update_status(message: str, progress: int):
    # print(f"{message}, {progress}")
    _update_status_text(message)
    _update_progress_bar(progress)


def _set_executing_status(is_executing: bool = worker_thread_running, is_initialized: bool = True,
                          is_completed: bool = False):
    global worker_thread_running
    worker_thread_running = is_executing

    _main_ui.pushButton_startStopProcessing.setEnabled(not is_executing and is_initialized)
    _main_ui.actionStart_Processing.setEnabled(not is_executing and is_initialized)

    if is_executing:
        _main_ui.progressBar_status.setValue(0)
    _main_ui.progressBar_status.setVisible(is_executing)

    _main_ui.pushButton_source.setEnabled(not is_executing)
    _main_ui.actionSelect_Source.setEnabled(not is_executing)

    _main_ui.tab_config.setEnabled(not is_executing)
    _main_ui.tab_result.setEnabled(not is_executing and is_completed)


def _apply_theme(new_theme: str = None):
    """
    apply theme to window

    Parameters
    ----------
    new_theme: str
        if provided, new theme will be written to program settings then apply this theme
        otherwise load theme from program settings and apply it
    """
    if new_theme is None:
        new_theme = program_settings.theme
    else:
        program_settings.set_theme(new_theme)
    if new_theme != '':
        _app.setStyleSheet(program_settings.get_theme_stylesheet())
        _mw.setWindowIcon(QtGui.QIcon(program_settings.get_theme_icon()))
    program_settings.to_json()


# FUNCTION FOR DEVELOPMENT (WILL BE REMOVED AFTER RELEASE)

# # test_result_display
class TestResultDisplay(QtCore.QThread):
    finish_signal = pyqtSignal()

    def run(self):
        src = Path(
            "source/extracted/GF1_PMS1_E122.3_N30.5_20210501_L1A0005630448/GF1_PMS1_E122.3_N30.5_20210501_L1A0005630448-MSS1.tiff")
        result_dir = PathSettings(Path("beta-output/"), source=Path('.1.txt'))
        global geo_transform_info, path_settings
        path_settings = result_dir
        geo_transform_info = GeoTransformer(src)
        self.finish_signal.emit()
        # result_set.test_signal.connect(_update_status)  # test&debug


tester_result_display_thread: TestResultDisplay


def tester_result_display():
    _set_executing_status(True)
    _update_status_text("Loading results")
    global tester_result_display_thread
    tester_result_display_thread = TestResultDisplay()
    tester_result_display_thread.finish_signal.connect(_on_result_return)
    tester_result_display_thread.start()


# # QThreadExceptionTest
class QThreadExceptionTest(QtCore.QThread):
    """
    exception from a thread can be handled by main thread
    """
    error_signal = pyqtSignal(Exception)

    def run(self):
        try:
            assert False, "raise a assertion exception"
        except Exception as ex:
            self.error_signal.emit(ex)


q_thread_exception_tester: QThreadExceptionTest


def q_thread_exception_test():
    global q_thread_exception_tester
    q_thread_exception_tester = QThreadExceptionTest()
    q_thread_exception_tester.error_signal.connect(raise_exception_in_main_thread)
    try:
        q_thread_exception_tester.start()
    except Exception as ex:
        print("the catch after .start()")
        raise_exception_in_main_thread(ex)


if __name__ == "__main__":
    # b = MainWindowController()
    sys.exit(init_exec())
    # print('sleeping')
    # time.sleep(10)
    # print('continue')
    # sys.exit(execute())
log_and_print(f"Working directory: {os.getcwd()}")
