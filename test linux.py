from __future__ import annotations
from datetime import datetime
import tifffile
from image_manipulate import tiff2png, save_as, SaveFormat, ImageCropper, format_shape_structure, transpose
import time
import pathlib


def get_shape():
    file = tifffile.TiffFile(tif_file)
    print(file.pages[0].shape)
    print(file.pages[0].tags['ImageLength'].value)
    print(file.pages[0].tags['ImageWidth'])


def subject1_convert_whole_image():
    print(f'{datetime.now()} start -> read tif')
    image_page = tifffile.TiffFile(tif_file).pages[0]
    image = image_page.asarray('memmap')
    order = format_shape_structure(image_page)
    if (0,1,2) != order:
        print(order)
        image = transpose(image, order)
        print(image.shape)
    print(f'{datetime.now()} read tif -> converting bit depth')
    image = tiff2png(image)
    # image = image_manipulate.tiff2png_convert(image)
    # print(image.shape)
    filename = "beta-output/whole.tiff"
    print(f'{datetimeti.now()} read tif -> converting bit depth -> saving image')
    save_as(_array=image, _type=SaveFormat.TIFF, filepath=pathlib.Path(filename))
    print(f"{datetime.now()} complete")
    cropper = ImageCropper(256, 256,
                           pathlib.Path('beta-output').joinpath(tif_file.name[:-len(tif_file.suffix)]),
                           SaveFormat.JPG)
    cropper.execute(tif_page=tifffile.TiffFile(filename).pages[0], crop_only=True)
    time.sleep(10)


# tif_file = pathlib.Path("src/GF2_PMS1_E122.1_N30.3_20190908_L1A0004235285-MSS1.tiff")
tif_file = pathlib.Path('source/extracted/GF6_PMS_E122.0_N29.8_20201014_L1A1120043330/GF6_PMS_E122.0_N29.8_20201014_L1A1120043330-MUX.tiff')
# subject1_convert_whole_image()
get_shape()