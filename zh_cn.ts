<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="main_window.ui" line="20"/>
        <source>MainWindow</source>
        <translation>程序</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="54"/>
        <location filename="main_window.ui" line="599"/>
        <source>Select Source</source>
        <translation>选择数据</translation>
    </message>
    <message>
        <source>Select XML</source>
        <translation type="vanished">选择XML</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="61"/>
        <source>Start</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="74"/>
        <source>Activity Log</source>
        <translation>程序日志</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="102"/>
        <source>Select &quot;source&quot; to continue</source>
        <translation>请选择数据</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="120"/>
        <source>Source:</source>
        <translation>数据源：</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="155"/>
        <source>Image Info</source>
        <translation>图像信息</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="166"/>
        <source>File Path</source>
        <translation>图像文件</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="182"/>
        <source>Select a file to continue</source>
        <translation>请选择文件</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="200"/>
        <source>Resolution</source>
        <translation>解析度</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="219"/>
        <source>Corner Coordinate</source>
        <translation>角落坐标</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="235"/>
        <source>Top Left</source>
        <translation>左上</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="242"/>
        <source>Top Right</source>
        <translation>右上</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="249"/>
        <source>Bottom Left</source>
        <translation>左下</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="256"/>
        <source>Bottom Right</source>
        <translation>右下</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="301"/>
        <source>Configurations</source>
        <translation>配置</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="313"/>
        <source>Requires Nvidia GPU supporting CUDA 11.</source>
        <translation>此选项需要支持CUDA11的英伟达显示卡。</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="316"/>
        <source>Enable CUDA Acceleration</source>
        <translation>启用CUDA加速</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="323"/>
        <source>Experimental. </source>
        <translation>试验性功能。 </translation>
    </message>
    <message>
        <location filename="main_window.ui" line="326"/>
        <source>Enhance Source Image (will require a lot of memory)</source>
        <translation>图片色彩增强（需要大量记忆体）</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="333"/>
        <source>Recommended for computers having less than 32 gigabyte of memory.</source>
        <translation>建议记忆体低于32G的电脑使用。</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="336"/>
        <source>Reduce memory usage (will require a lot of disk space)</source>
        <translation>降低记忆体使用量（需要使用大量磁碟空间）</translation>
    </message>
    <message>
        <source>Enhance Source Image</source>
        <translation type="vanished">图片色彩增强</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="370"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="397"/>
        <source>Process Result</source>
        <translation>处理结果</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="449"/>
        <source>Result will be displayed here</source>
        <translation>处理结果在此展示</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="465"/>
        <source>Export Results</source>
        <translation>结果导出</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="471"/>
        <source>Save Sheet Data As...</source>
        <translation>导出表格…</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="478"/>
        <source>Export Whole Image As...</source>
        <translation>合并并导出图片…</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="529"/>
        <source>Ready</source>
        <translation>就绪</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="562"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="572"/>
        <location filename="main_window.ui" line="594"/>
        <source>Preference</source>
        <translation>设定配置</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="579"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="589"/>
        <source>Acticity Log</source>
        <translation>程序日志</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="607"/>
        <source>Start Processing</source>
        <translation>开始处理</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="615"/>
        <source>Stop Processing</source>
        <translation>停止处理</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="620"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="625"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
</context>
</TS>
