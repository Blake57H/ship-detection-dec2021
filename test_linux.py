from __future__ import annotations

import math
from datetime import datetime
import tifffile
from tifffile import TiffFile
from image_manipulate import tiff2png, save_as, SaveFormat, ImageCropper, format_shape_structure, transpose,\
    uint16_to_uint8_ver2, img_enhance, clean_temporary_folder
import time
import pathlib


def get_shape():
    file = tifffile.TiffFile(tif_file)
    print(file.pages[0].shape)
    print(file.pages[0].tags['ImageLength'].value)
    print(file.pages[0].tags['ImageWidth'])


def subject1_convert_whole_image():
    print(f'{datetime.now()} start -> read tif')
    image_page = tifffile.TiffFile(tif_file).pages[0]
    image = image_page.asarray('memmap')
    order = format_shape_structure(image_page)
    if (0,1,2) != order:
        print(order)
        image = transpose(image, order)
        print(image.shape)
    print(f'{datetime.now()} read tif -> converting bit depth')
    image = tiff2png(image)
    # image = uint16_to_uint8_ver2(image, True)
    # image = image_manipulate.tiff2png_convert(image)
    # print(image.shape)
    filename = "beta-output/whole.tiff"
    print(f'{datetime.now()} read tif -> converting bit depth -> saving image')
    save_as(_array=image, _type=SaveFormat.TIFF, filepath=pathlib.Path(filename))
    print(f"{datetime.now()} complete")
    cropper = ImageCropper(256, 256,
                           pathlib.Path('beta-output').joinpath(tif_file.name[:-len(tif_file.suffix)]),
                           SaveFormat.JPG)
    cropper.execute(tif_page=tifffile.TiffFile(filename).pages[0], crop_only=True)
    time.sleep(10)


def subject1_convert_whole_image_ver2():
    string = 'start -> read tif '
    print(f'{datetime.now()} {string}')
    global tif_file
    image_page = tifffile.TiffFile(tif_file).pages[0]
    output = pathlib.Path('temp')
    ''''''
    height_value = image_page.tags['ImageLength'].value
    width_value = image_page.tags['ImageWidth'].value
    ''''''
    print(f"{datetime.now()} {string}<- image size is {width_value}x{height_value}")
    clean_temporary_folder(output.__str__())

    # 7000 by 7000 is my laptop's ram limitation
    the_switch = height_value > 7000 or width_value > 7000
    print(the_switch)
    the_switch = False

    if the_switch:
        cropper = ImageCropper(math.ceil(width_value / 2), math.ceil(height_value / 2),
                               output,
                               SaveFormat.TIFF)
        string += '-> split to 4 pics and enhance '
        ''''''
    else:
        cropper = ImageCropper(width_value, height_value,
                               output,
                               SaveFormat.TIFF)
        string += '-> enhancing '

    print(f'{datetime.now()} {string}')
    # image = uint16_to_uint8_ver2(image_page.asarray(), drop_alpha=True)
    # save_as(output.joinpath("a.tiff"), SaveFormat.TIFF, image)
    cropper.execute(image_page, crop_only=False)
    files = output.glob('*.tiff')
    # files = [tif_file.__str__()]
    output = pathlib.Path('beta-output').joinpath(tif_file.name[:-len(tif_file.suffix)])
    cropper = ImageCropper(256, 256, None, SaveFormat.JPG)
    counter = 0
    for file in files:
        counter += 1

        if the_switch:
            some_string = counter.__str__()
            cropper.set_output_folder(output.joinpath(some_string))
            cropper.set_filename_postfix(some_string)
        else:
            cropper.set_output_folder(output)

        print(f'{datetime.now()} {string}-> cutting pic {counter}')
        cropper.execute(TiffFile(file).pages[0], crop_only=True, sub_directory_count=(0 if the_switch else 4))
    string += f'-> cut {counter} pic -> complete'
    print(f'{datetime.now()} {string}')
    # time.sleep(10)


tif_file = pathlib.Path("src/GF2_PMS2_E122.7_N30.8_20210823_L1A0005835208-MSS2.tiff")
# tif_file = pathlib.Path('source/extracted/GF6_PMS_E122.0_N29.8_20201014_L1A1120043330/GF6_PMS_E122.0_N29.8_20201014_L1A1120043330-MUX.tiff')
subject1_convert_whole_image_ver2()
# get_shape()
