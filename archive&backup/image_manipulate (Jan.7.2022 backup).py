import os
import pathlib
import shutil
import xml.etree.ElementTree as eT
from typing import Union, Tuple, List

import math
import numpy as np
import tifffile
from cv2 import cv2
from tifffile import TiffPage


class SaveFormat:
    """
    Currently implemented image format to save
    """
    PNG = '.png'
    RGBA_PNG = '_rgba.png'
    TIFF = '.tiff'
    JPG = '.jpg'


# 16位转8位
def normalization(data):
    _range = np.max(data) - np.min(data)
    return (data - np.min(data)) / _range


def uint16_to_uint8(img_16):
    if np.max(img_16) - np.min(img_16) != 0:
        # img_nrm = (img_16 - np.min(img_16)) / (np.max(img_16) - np.min(img_16)) #计算灰度范围,归一化
        img_nrm = normalization(img_16)
        img_8 = np.uint8(255 * img_nrm)
    return img_8


def uint16_to_uint8_ver2(img_16, drop_alpha: bool = False):
    # print(np.min(img_16))
    if drop_alpha:
        img_16 = img_16[:,:,:3]
    max_val = np.max(img_16)
    if max_val <= 255:
        bit = 8
    elif max_val <= 1023:
        max_val = 1023
        bit = 10
    elif max_val <= 4095:
        max_val = 4095
        bit = 12
    else:
        raise Exception(f"Unknown image bit-depth (max_val={max_val} > 12-bit), expected 8/10/12 bit")
    if bit > 8:
        print(f"Assuming {bit} bit color depth")
        img_8 = np.array((img_16 / max_val * 255), dtype='uint8')
    elif img_16.dtype == 'uint8':
        img_8 = img_16
    else:
        img_8 = np.array(img_16, dtype='uint8')
    # print(np.max(img_8))
    # print(np.min(img_8))
    # print(img_8)
    # exit(0)
    return img_8


# 图像颜色处理
def img_enhance(rgba, ratio=4, radius=2):  # rgb三通道分别增强，ratio是对比度增强因子，radius是卷积模板半径

    def stretchImage(data, s=0.01, bins=2000):  # 线性拉伸，去掉最大最小0.5%的像素值，然后线性拉伸至[0,1]
        ht = np.histogram(data, bins);
        d = np.cumsum(ht[0]) / float(data.size)
        lmin = 0
        lmax = bins - 1
        while lmin < bins:
            if d[lmin] >= s:
                break
            lmin += 1
        while lmax >= 0:
            if d[lmax] <= 1 - s:
                break
            lmax -= 1
        return np.clip((data - ht[1][lmin]) / (ht[1][lmax] - ht[1][lmin]), 0, 1)

    g_para = {}

    def getPara(radius=5):  # 根据半径计算权重参数矩阵
        # global g_para
        m = g_para.get(radius, None)
        if m is not None:
            return m
        size = radius * 2 + 1
        m = np.zeros((size, size))
        for h in range(-radius, radius + 1):
            for w in range(-radius, radius + 1):
                if h == 0 and w == 0:
                    continue
                m[radius + h, radius + w] = 1.0 / math.sqrt(h ** 2 + w ** 2)
        m /= m.sum()
        g_para[radius] = m
        return m

    def zmIce(I, ratio=4, radius=300):  # 常规的ACE实现
        para = getPara(radius)
        height, width = I.shape
        zh, zw = [0] * radius + list(range(height)) + [height - 1] * radius, [0] * radius + list(range(width)) + [
            width - 1] * radius
        Z = I[np.ix_(zh, zw)]
        res = np.zeros(I.shape)
        for h in range(radius * 2 + 1):
            for w in range(radius * 2 + 1):
                if para[h][w] == 0:
                    continue
                res += (para[h][w] * np.clip((I - Z[h:h + height, w:w + width]) * ratio, -1, 1))
        return res

    def zmIceFast(I, ratio, radius):  # 单通道ACE快速增强实现
        height, width = I.shape[:2]
        if min(height, width) <= 2:
            return np.zeros(I.shape) + 0.5
        Rs = cv2.resize(I, ((width + 1) // 2, (height + 1) // 2))
        Rf = zmIceFast(Rs, ratio, radius)  # 递归调用
        Rf = cv2.resize(Rf, (width, height))
        Rs = cv2.resize(Rs, (width, height))

        return Rf + zmIce(I, ratio, radius) - zmIce(Rs, ratio, radius)

    # r, g, b = rgba[:, :, 0], rgba[:, :, 1], rgba[:, :, 2]  # rgb or bgr (todo)
    # img = np.dstack([b, g, r])
    img = np.dstack([rgba[:, :, 0], rgba[:, :, 1], rgba[:, :, 2]])
    
    img = img / 255.0
    res = np.zeros(img.shape)
    # 自动彩色均衡
    for k in range(3):
        res[:, :, k] = stretchImage(zmIceFast(img[:, :, k], ratio, radius))

    res = res[:, :, ::-1]

    # 伽马变换
    img_gamma = np.power(res, 0.75) * 255.0
    img_gamma = img_gamma.astype(np.uint8)

    return img_gamma


def tiff2png(img_16):
    # img_8 = uint16_to_uint8(img_16)
    img_8 = uint16_to_uint8_ver2(img_16)
    rgb = img_enhance(img_8)
    return rgb


def drop_alpha(rgba):
    return np.dstack([rgba[:, :, 0], rgba[:, :, 1], rgba[:, :, 2]])


def transpose(array, order):
    return np.transpose(array, order)


class GeoTransformer:
    def __init__(self, xml_path: pathlib.Path, width_and_height: Tuple[int, int] = None):
        """
        Parameters
        ----------

        xml_path: pathlib.Path()
            path to the xml file(tested), or path to the tif image (expecting xml file next to it)

        width_and_height: (int, int)
            image width and height, in pixel
        """
        if xml_path.suffix != '.xml':
            xml_path = pathlib.Path(xml_path.__str__()[:-len(xml_path.suffix)] + '.xml')
        self._check_file(xml_path)

        # read the corners' coordinate
        # tutorial for parsing xml: https://stackoverflow.com/questions/1912434/how-to-parse-xml-and-count-instances-of-a-particular-node-attribute
        # note: if xml is untrusted (means you are not 100% confident that the xml file is malicious), use 'defusedxml',
        # and read more here: https://docs.python.org/3/library/xml.html#xml-vulnerabilities. In my case I simply
        # don't care.
        root = eT.parse(xml_path).getroot()
        self.tl = (float(root.findtext('TopLeftLongitude')), float(root.findtext('TopLeftLatitude')))
        self.tr = (float(root.findtext('TopRightLongitude')), float(root.findtext('TopRightLatitude')))
        self.bl = (float(root.findtext('BottomLeftLongitude')), float(root.findtext('BottomLeftLatitude')))
        self.br = (float(root.findtext('BottomRightLongitude')), float(root.findtext('BottomRightLatitude')))

        # read image resolution (in pixel)
        if width_and_height is None:
            width_and_height = (int(root.findtext('WidthInPixels')), int(root.findtext('HeightInPixels')))

        # calculate geo transform (I think....)
        # given there are two sets of values use the average as final geo transform value (idk, feeling like it)
        gt = np.zeros(6)
        gt[0], gt[3] = self.tl
        gt[1] = ((self.tr[0] - self.tl[0]) + (self.br[0] - self.bl[0])) / 2 / width_and_height[0]
        gt[2] = ((self.bl[0] - self.tl[0]) + (self.br[0] - self.tr[0])) / 2 / width_and_height[1]
        gt[4] = ((self.tr[1] - self.tl[1]) + (self.br[1] - self.bl[1])) / 2 / width_and_height[0]
        gt[5] = ((self.bl[1] - self.tl[1]) + (self.br[1] - self.tr[1])) / 2 / width_and_height[1]
        self.geo_transform = gt
        self.resolution = width_and_height

    def __str__(self):
        return (f"Corner coordinates:\n"
                f"Top left: {self.tl}\n"
                f"Top right: {self.tr}\n"
                f"Bottom left: {self.bl}\n"
                f"Bottom right: {self.br}"
                f"\n\n"
                f"Geo transform:\n"
                f"{self.geo_transform}"
                f"\n\n"
                f"Image resolution (width * height):\n"
                f"{self.resolution[0]}x{self.resolution[1]}")

    @staticmethod
    def _check_file(xml_path: Union[str, pathlib.Path]) -> None:
        """
        check given file is:
            (1) exists
            (2) not a path
        """
        if not pathlib.Path(xml_path).exists():
            raise FileNotFoundError(f"file not found: {xml_path}")
        if not pathlib.Path(xml_path).is_file():
            raise IsADirectoryError(f"{xml_path} is a directory")

    def get_geo_transform(self) -> Tuple[float]:
        return self.geo_transform

    def pixel_to_decimal_geo(self, x: int, y: int) -> Tuple[float, float]:
        """
        https://gdal.org/user/raster_data_model.html#affine-geotransform
        Xgeo = GT(0) + Xpixel*GT(1) + Yline*GT(2)
        Ygeo = GT(3) + Xpixel*GT(4) + Yline*GT(5)

        x_geo = longitude, y_geo = latitude
        """
        x_geo: float = self.geo_transform[0] + x * self.geo_transform[1] + y * self.geo_transform[2]
        y_geo: float = self.geo_transform[3] + x * self.geo_transform[4] + y * self.geo_transform[5]
        return x_geo, y_geo

    def decimal_geo_to_pixel(self, x_geo: float, y_geo: float) -> Tuple[int, int]:
        """
        https://gdal.org/user/raster_data_model.html#affine-geotransform
        Xgeo = GT(0) + Xpixel*GT(1) + Yline*GT(2)
        Ygeo = GT(3) + Xpixel*GT(4) + Yline*GT(5)

        x_geo = longitude, y_geo = latitude
        """
        a = self.geo_transform[2] / self.geo_transform[5]

        # SAY GT(2) = a*GT(5)
        # Xgeo - Ygeo*a = GT(0) + Xpixel*GT(1) + Yline*GT(2) - a*(GT(3) + Xpixel*GT(4) + Yline*GT(5))
        # SAY Xgeo - Ygeo*a AS var_x, GT(0) + Xpixel*GT(1) + Yline*GT(2) - a*(GT(3) + Xpixel*GT(4) + Yline*GT(5)) AS var_y
        # THEN var_x = var_y
        # MOVE GT0, GT3 TO THE LEFT, I GET: var_x - GT(0) + a*GT(3)
        #   = (Xpixel*GT(1) + Yline*GT(2)) - a*(Xpixel*GT(4) + Yline*GT(5))
        #   = Xpixel*GT(1) + Yline*GT(2) - a*Xpixel*GT(4) - a*Yline*GT(5)
        #   = Xpixel*GT(1) - a*Xpixel*GT(4)  [because Yline*GT(2) - a*Yline*GT(5) = 0]
        # GROUP: Xpixel * (GT(1) - a*GT(4)) =  Xgeo - Ygeo*a - GT(0) + a*GT(3)
        # FINALLY: Xpixel = (Xgeo - Ygeo*a - GT(0) + a*GT(3)) / (GT(1) - a*GT(4))
        var_x = x_geo - y_geo * a




def get_cropped(page: TiffPage,
                start_width: int,
                start_height: int,
                crop_size_width: int,
                crop_size_height: int,
                formatted_page_shape: tuple,
                shape_structure: List[str] = None,
                bypass_error: bool = True) -> np.array:
    """

    Parameters
    ----------

    Returns
    -------
    """

    v = []  # will be used in cropping image
    o = []
    height, width, channels = formatted_page_shape
    # print(formatted_page_shape)

    if start_height < 0 or \
            start_height + crop_size_height > height \
            or start_width < 0 or \
            start_width + crop_size_width > width:
        if bypass_error:
            start_height = max(0, start_height)
            start_width = max(0, start_width)
            crop_size_height = min(height - start_height, crop_size_height)
            crop_size_width = min(width - start_width, crop_size_width)
        else:
            raise ValueError("Requested crop area is out of image bounds.")

    if shape_structure is None:
        shape_structure = ['h', 'w', 'c']
    elif len(shape_structure) != 3:
        raise ValueError("Invalid shape structure length")
    elif 'h' not in shape_structure or 'w' not in shape_structure or 'c' not in shape_structure:
        raise ValueError("Incorrect shape structure content")

    order = [0, 1, 2]
    for d in shape_structure:
        if d == 'h':
            v.append(start_height)
            v.append(start_height + crop_size_height)
            order[0] = shape_structure.index(d)
        elif d == 'w':
            v.append(start_width)
            v.append(start_width + crop_size_width)
            order[1] = shape_structure.index(d)
        elif d == 'c':
            v.append(0)
            v.append(channels)
            order[2] = shape_structure.index(d)
    # print(order)

    _test = page.asarray('memmap')  # use memmap to avoid memory overflow, especially for really large tiff image

    # tiff image for the project I'm involved shapes (height, width, channel)
    save = _test[v[0]:v[1], v[2]:v[3], v[4]:v[5]]
    if shape_structure != ['h', 'w', 'c']:
        save = np.transpose(save, (order[0], order[1], order[2]))

    # print(f"DEBUG: {page.shape} -> {save.shape}")
    # exit(0)

    return np.array(save)


def format_shape_structure(tif_page: TiffPage):
    """
    change any order of channel, width, height to  [height, width, channels]
    """

    height, width, channels = tif_page.shape
    height_value = tif_page.tags['ImageLength'].value
    width_value = tif_page.tags['ImageWidth'].value
    shape_structure_dict = {}

    if height_value != height:
        if width_value == height:
            shape_structure_dict['w'] = 0
        if height_value == width:
            shape_structure_dict['h'] = 1
            if shape_structure_dict.get('w') is None:
                shape_structure_dict['w'] = 2
            # height, width = self.__swap_variable(height, width)
        elif height_value == channels:
            shape_structure_dict['h'] = 2
            if shape_structure_dict.get('w') is None:
                shape_structure_dict['w'] = 1
            # height, channels = self.__swap_variable(height, channels)
    else:
        shape_structure_dict['h'] = 0
        if width_value != width:
            # width, channels = self.__swap_variable(width, channels)
            shape_structure_dict['w'] = 2
        else:
            shape_structure_dict['w'] = 1
    shape_structure_dict['c'] = 3 - shape_structure_dict['h'] - shape_structure_dict['w']
    return shape_structure_dict['h'], shape_structure_dict['w'], shape_structure_dict['c']


def save_as(filepath: pathlib.Path, _type: SaveFormat, _array: np.array, color: str = "RGB;8"):
    """
    Save an array as image
    :param filepath: file path, pathlib.Path
    :param _type: image format, refer to SaveFormat
    :param _array: the array to save as image
    :param color: pypng color
    :return: nothing
    """
    if filepath.exists():
        if filepath.is_file():
            pass
            # warn(f"{filepath.__str__()} exists and will be over-written")
        else:
            raise FileExistsError(f"a directory named {filepath.__str__()} exists, remove it before continuing")

    '''
    _temp = filepath
    _dir_stack = list()
    while _temp.parent.__str__() != '' and not _temp.parent.exists():
        _dir_stack.append(_temp.parent)
        _temp = _temp.parent
    for _ in len(_dir_stack):
        _dir_stack.pop().mkdir()
    '''
    if not filepath.parent.exists():
        filepath.parent.mkdir(parents=True)

    if _type == SaveFormat.PNG:
        """
        save_array = []
        for row in _array:
            r = []
            for column in row:
                for element in column:
                    r.append(element)
            save_array.append(r)
        save_array = np.array(save_array)
        png.from_array(save_array, color).save(filepath.__str__())
        """
        cv2.imwrite(filepath.__str__(), cv2.cvtColor(_array, cv2.COLOR_RGB2BGR), [int(cv2.IMWRITE_PNG_COMPRESSION), 9])
        # png.from_array(_array, color).save(filepath.__str__())
    elif _type == SaveFormat.TIFF:
        tifffile.imwrite(filepath.__str__(), _array)
    elif _type == SaveFormat.JPG:
        cv2.imwrite(filepath.__str__(), cv2.cvtColor(_array, cv2.COLOR_RGB2BGR), [int(cv2.IMWRITE_JPEG_QUALITY), 100])
        # cv2.imwrite(filepath.__str__(), _array, [int(cv2.IMWRITE_JPEG_QUALITY), 100])
    else:
        raise ValueError("Unrecognized type, use options offered in class SaveFormat")


def clean_temporary_folder(specific_directory):
    """
    Delete all folder and files in a directory (used to clear temporary files)
    You are allowed to delete other things as well
    :param specific_directory: the folder you wanna clean
    :return: nothing
    """
    if not os.path.exists(specific_directory):
        return
    assert os.path.isdir(specific_directory), f"{specific_directory} is not a directory"
    for filename in os.listdir(specific_directory):
        file_path = os.path.join(specific_directory, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))


class ImageCropper:
    def __init__(self,
                 crop_width: int,
                 crop_height: int,
                 output_folder: pathlib.Path,
                 output_format: SaveFormat,
                 sub_directory_count: int = 0,
                 filename_postfix: str = None
                 ):
        self.crop_width = crop_width
        self.crop_height = crop_height
        self.output_folder = output_folder
        self.output_format = output_format
        self.sub_directory_count = sub_directory_count
        self._filename_postfix = filename_postfix

    @staticmethod
    def __swap_variable(var1, var2):
        """
        a simple method returning swapped variable

        Parameters
        ----------
            var1, var2

        Returns
        -------
            var2, var1
        """
        return var2, var1

    @staticmethod
    def __swap_array(array, index1, index2):
        array[index1], array[index2] = array[index2], array[index1]
        return array

    def set_output_folder(self, path: pathlib.Path):
        self.output_folder = path

    def set_filename_postfix(self, postfix: str):
        self._filename_postfix = postfix

    def get_postfix(self) -> str:
        if self._filename_postfix is None:
            return ''
        return f"_{self._filename_postfix}"

    def execute(self, tif_page: TiffPage, crop_only: bool = False, sub_directory_count: int = 0,
                enhance_first: bool = False):
        """
        Parameters
        ----------

        tif_page: TiffPage
            A page from tif

        crop_only: bool
            If true, crop image without depth scaling or

        Returns
        -------
        out
            i donno, maybe no return
        """
        if len(tif_page.shape) != 3:
            raise ValueError("tif page must be shaped like a 3d array")

        height, width, channels = tif_page.shape
        height_value = tif_page.tags['ImageLength'].value
        width_value = tif_page.tags['ImageWidth'].value

        shape_structure = ['o', 'o', 'o']

        _h, _w, _c = format_shape_structure(tif_page)
        """        
        shape_structure_dict = {}

        if height_value != height:
            if width_value == height:
                shape_structure_dict['w'] = 0
            if height_value == width:
                shape_structure_dict['h'] = 1
                if shape_structure_dict.get('w') is None:
                    shape_structure_dict['w'] = 2
                # height, width = self.__swap_variable(height, width)
            elif height_value == channels:
                shape_structure_dict['h'] = 2
                if shape_structure_dict.get('w') is None:
                    shape_structure_dict['w'] = 1
                # height, channels = self.__swap_variable(height, channels)
        else:
            shape_structure_dict['h'] = 0
            if width_value != width:
                # width, channels = self.__swap_variable(width, channels)
                shape_structure_dict['w'] = 2
            else:
                shape_structure_dict['w'] = 1
        shape_structure_dict['c'] = 3 - shape_structure_dict['h'] - shape_structure_dict['w']
        """

        shape_structure[_h] = 'h'
        shape_structure[_w] = 'w'
        shape_structure[_c] = 'c'

        fps = [height, width, channels]  # fps = Formatted Page Shape
        '''
        height, width, channels = \
            fps[shape_structure_dict['h']], fps[shape_structure_dict['w']], fps[shape_structure_dict['c']]
        '''
        height, width, channels = fps[_h], fps[_w], fps[_c]
        fps = (height, width, channels)

        expected_segment_count = math.ceil(height / self.crop_height) * math.ceil(width / self.crop_width)
        tiles_per_dir = expected_segment_count
        if expected_segment_count < 30:
            print_per_segment = 1
        else:
            print_per_segment = 10
        segment = 0
        sub_directory_index = 1
        if sub_directory_count > 0:
            tiles_per_dir = math.ceil(expected_segment_count / sub_directory_count)
            self.output_folder = self.output_folder.joinpath(sub_directory_index.__str__())
        print(f"Expecting {expected_segment_count} segment(s)")
        '''
        if sub_directory_count > 0:
            tiles_per_dir = math.ceil(expected_segment_count/tiles_per_dir)
        '''
        row_index = column_index = 0
        while row_index * self.crop_height < height:
            image = get_cropped(
                page=tif_page,
                start_width=column_index * self.crop_width,
                start_height=row_index * self.crop_height,
                crop_size_width=self.crop_width,
                crop_size_height=self.crop_height,
                formatted_page_shape=fps,
                shape_structure=shape_structure
            )
            image = uint16_to_uint8_ver2(image, drop_alpha=True)
            # print(image.shape)
            if not crop_only:
                image = img_enhance(image)
            # print(image.shape)
            filename = f"r{row_index}_c{column_index}{self.get_postfix()}{self.output_format}"
            save_as(_array=image, _type=self.output_format, filepath=self.output_folder.joinpath(filename))
            segment += 1
            if segment % print_per_segment == 0:
                print(f"\r{round(segment * 100 / expected_segment_count, ndigits=1)}% complete", end='')
            if segment % tiles_per_dir == 0:
                sub_directory_index += 1
                self.output_folder = self.output_folder.parent.joinpath(sub_directory_index.__str__())
            column_index += 1
            if column_index * self.crop_width >= width:
                column_index = 0
                row_index += 1
        print()


if __name__ == "__main__":
    test = np.random.rand(500, 500, 4)
    result = tiff2png(test)
    print(result.shape)
