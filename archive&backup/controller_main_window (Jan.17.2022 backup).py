import os
import traceback
from pathlib import Path
from threading import Thread
from typing import List
import gc

import sys
import tifffile
from PyQt6 import QtCore, QtWidgets, QtGui
from PyQt6.QtCore import pyqtSignal

import view_main_window
from execute_model import DetectModel
from general_functions import show_warning, LanguagePack, log_function, get_time_as_filename, clean_temporary_folder
from image_manipulate import GeoTransformer, ImageCropperReversed, SaveFormat


def exception_handler(etype, value, tb):
    print("exception_handler called")  # DEBUG
    message_lines = traceback.format_exception(etype, value, tb)
    reason_of_error = message_lines[len(message_lines)-1].strip()
    error_msg = ''.join(message_lines)
    # error_msg = etype.__str__() + ";" + value.__str__() + ";" + tb.__str__()
    log_and_print(error_msg)
    # todo: show a messagebox and reset program
    msg_box = QtWidgets.QMessageBox()
    msg_box.setWindowTitle(lp.error_popup_title)
    msg_box.setText(lp.error_popup_body)
    msg_box.setInformativeText(reason_of_error)
    msg_box.setIcon(QtWidgets.QMessageBox.Icon.Critical)
    msg_box.setStandardButtons(QtWidgets.QMessageBox.StandardButton.Reset|QtWidgets.QMessageBox.StandardButton.Abort)
    msg_box.setDetailedText(error_msg)
    msg_box.buttonClicked.connect(on_error_msg_box_button_clicked)
    x = msg_box.exec()


def raise_exception_in_main_thread(exception: Exception):
    """
    This function should be called only from main thread
    """
    raise exception


def display_error_message_box(reason_of_error, error_msg):
    msg_box = QtWidgets.QMessageBox()
    msg_box.setWindowTitle(lp.error_popup_title)
    msg_box.setText(lp.error_popup_body)
    msg_box.setInformativeText(reason_of_error)
    msg_box.setIcon(QtWidgets.QMessageBox.Icon.Critical)
    msg_box.setStandardButtons(QtWidgets.QMessageBox.StandardButton.Reset|QtWidgets.QMessageBox.StandardButton.Abort)
    msg_box.setDetailedText(error_msg)
    msg_box.buttonClicked.connect(on_error_msg_box_button_clicked)
    x = msg_box.exec()


def on_error_msg_box_button_clicked(i: QtWidgets.QPushButton):
    print(i == QtWidgets.QMessageBox.StandardButton.Reset)
    print(i.text())
    if i.text() == 'Abort':
        _app.quit()
    elif i.text() == 'Reset':
        pass
        # a reset function here todo:


class MainWindow(QtWidgets.QMainWindow):
    # noinspection PyPep8Naming
    def closeEvent(self, QCloseEvent):
        if can_exit():
            QCloseEvent.accept()
        else:
            QCloseEvent.ignore()


class PathSettings:
    def __init__(self, output_folder: Path, source: Path = None):
        self.output_folder_parent = output_folder
        self.source: Path = source

    def _can_return(self) -> bool:
        return self.source is not None

    def set_input_file(self, source: Path):
        self.source = source

    def get_output_base_directory(self) -> Path:
        if self._can_return():
            return self.output_folder_parent.joinpath(self.source.name[:-len(self.source.suffix)])

    def get_unmarked_image_directory(self) -> Path:
        return self.get_output_base_directory().joinpath('cropped')

    def get_marked_image_directory(self) -> Path:
        return self.get_output_base_directory().joinpath('image_out')

    def get_marked_text_directory(self) -> Path:
        return self.get_output_base_directory().joinpath('text_out')


class ResultSet(QtCore.QAbstractTableModel):
    finished_signal = QtCore.pyqtSignal()

    # test_signal = QtCore.pyqtSignal(str, int)

    class ResultItem:
        longitude: float
        latitude: float
        confidence: float
        marked_image: str

        def __init__(self, longitude: float, latitude: float, confidence: float, marked_image: str):
            self.longitude = longitude
            self.latitude = latitude
            self.confidence = confidence
            self.marked_image = marked_image

        def __str__(self):
            return f"longitude={self.longitude}; latitude={self.latitude}; confidence={self.confidence}; image={self.marked_image}"

        # def index(self, i):
        #     if i == 0:
        #         return self.index
        #     elif i == 1:
        #         return self.longitude
        #     elif i == 2:
        #         return self.latitude
        #     else:
        #         return QtCore.QVariant()

        @staticmethod
        def get_header():
            return ['index', 'longitude', 'latitude']

    def __init__(self, path_settings_base: PathSettings):
        super().__init__()
        self.unmarked_image_segments = path_settings_base.get_unmarked_image_directory()
        self.marked_image_segments = path_settings_base.get_marked_image_directory()
        self.marked_text = path_settings_base.get_marked_text_directory()
        self.result_list: List[ResultSet.ResultItem] = list()
        return

    def setup_list(self):
        # validating
        assert self.unmarked_image_segments.exists() and self.unmarked_image_segments.is_dir(), \
            "cannot find unmarked results"
        assert self.marked_image_segments.exists() and self.marked_image_segments.is_dir(), \
            "cannot find marked results"
        assert self.unmarked_image_segments.exists() and self.unmarked_image_segments.is_dir(), \
            "cannot find text results"
        assert geo_transform_info is not None, "Geo info data hasn't initialized. Setup 'GeoTransformer' first."

        # load data
        for item in self.marked_text.glob('*.txt'):
            marked_image = self.marked_image_segments.joinpath(item.name[:-len(item.suffix)] + SaveFormat.JPG).__str__()
            with open(item, 'r') as stream:
                while True:
                    line = stream.readline()

                    if line is None or line.strip() == '':
                        break

                    values = line.split()
                    if len(values) != 5:
                        show_warning("Unrecognized result text. Has it updated?")
                        continue
                    x = int((int(values[0]) + int(values[2])) / 2)
                    y = int((int(values[1]) + int(values[3])) / 2)
                    x_geo, y_geo = geo_transform_info.pixel_to_decimal_geo_with_filename(Path(item).name, x, y, 256)
                    self.result_list.append(self.ResultItem(x_geo, y_geo, float(values[4]), marked_image))
        self.finished_signal.emit()
        # self.test_signal.emit("hi", 100)

    def get_all(self):
        return self.result_list

    def get(self, index: int) -> ResultItem:
        return self.result_list[index]

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self.result_list)

    def columnCount(self, parent=None, *args, **kwargs):
        return len(self.ResultItem.get_header())

    def headerData(self, column, Qt_Orientation, role=None):
        # print(column)
        # print(self.ResultItem.get_header())
        # print(Qt_Orientation)
        # print(role == QtCore.Qt.ItemDataRole.DisplayRole)
        if Qt_Orientation == QtCore.Qt.Orientation.Horizontal and role == QtCore.Qt.ItemDataRole.DisplayRole:
            return self.ResultItem.get_header()[column]
        # else:
        #     return QtCore.QVariant()

    def data(self, QModelIndex: QtCore.QModelIndex, role=None):
        if self.rowCount() == 0 or QModelIndex.row() >= self.rowCount() or \
                role != QtCore.Qt.ItemDataRole.DisplayRole or QModelIndex.column() >= self.columnCount():
            return QtCore.QVariant()
        else:
            # self.index(row = QModelIndex.row(), column=QModelIndex.column())
            if QModelIndex.column() == 0:
                return QModelIndex.row()
            if QModelIndex.column() == 1:
                return self.result_list[QModelIndex.row()].longitude.__str__()
            if QModelIndex.column() == 2:
                return self.result_list[QModelIndex.row()].latitude.__str__()


def on_table_selection_changed(selected: QtCore.QItemSelection, deselected: QtCore.QItemSelection):
    # if selected.indexes().__len__() != 0:
    #     print(selected.indexes()[0].row())
    # else:
    #     print(selected.indexes())
    # if deselected.indexes().__len__() != 0:
    #     print(deselected.indexes()[0].row())
    # else:
    #     print(deselected.indexes())
    # DEBUG

    # update image display if it is a new row
    if selected.indexes().__len__() != 0 or \
            deselected.indexes().__len__() != 0 and selected.indexes()[0].row() != deselected.indexes()[0].row():
        _main_ui.label_resultImageView.setPixmap(
            QtGui.QPixmap(result_set.get(selected.indexes()[0].row()).marked_image)
        )


class WorkerSJob(QtCore.QThread):
    job_abort_signal = pyqtSignal(Exception)
    job_complete_signal = pyqtSignal()
    job_progress_update_signal = pyqtSignal(str, int)

    def __init__(self, enable_cuda: bool, enhance_image: bool, clean_up_output_path: bool = True):
        super().__init__()
        self.enable_cuda = enable_cuda
        self.enhance_image = enhance_image
        self.clean_up_output_path = clean_up_output_path

    def run(self):
        try:
            if self.clean_up_output_path:
                clean_temporary_folder(path_settings.output_folder_parent.__str__())

            # _setup_cropper(path_settings.get_unmarked_image_directory())
            cropper = ImageCropperReversed(
                crop_width=256, crop_height=256,
                output_folder=path_settings.get_unmarked_image_directory(),
                output_format=SaveFormat.JPG
            )
            cropper.update_progress_callback.connect(self.report_progress)
            cropper.execute(tifffile.TiffFile(path_settings.source.__str__()).pages[0],
                            crop_only=not self.enhance_image)
            # print('CROPPER COMPLETE')  # debug

            self.report_progress(lp.detecting_ships, 0)

            dm = DetectModel(self.enable_cuda)
            dm.update_progress_signal.connect(self.report_progress_progress_int)
            # print("DETECT MODEL START")  # debug
            dm.run(
                input=path_settings.get_unmarked_image_directory().__str__(),
                image_output=path_settings.get_marked_image_directory().__str__(),
                text_output=path_settings.get_marked_text_directory().__str__()
            )

            self.report_progress_progress_str(lp.displaying_result)
            result_set = ResultSet(path_settings)
            result_set.setup_list()
            self.job_complete_signal.emit()
        except Exception as ex:
            # (_type, value, _traceback) = sys.exc_info()
            # sys.excepthook(_type, value, _traceback)
            self.job_abort_signal.emit(ex)

    def report_progress(self, message: str, progress: int):
        self.job_progress_update_signal.emit(message, progress)

    def report_progress_progress_int(self, progress):
        self.job_progress_update_signal.emit(None, progress)

    def report_progress_progress_str(self, message):
        self.job_progress_update_signal.emit(message, None)


# VARIABLES

_main_ui: view_main_window.Ui_MainWindow
# _source: Path
_mw: QtWidgets.QMainWindow
_app = QtWidgets.QApplication
geo_transform_info: GeoTransformer
# cropper: ImageCropperReversed
worker_thread: WorkerSJob  # you're a slave here (evil laughing)
worker_thread_running = False
lp = LanguagePack
log_filename = Path('logs').joinpath(f'{get_time_as_filename()}.txt')
# noinspection PyPep8Naming
result_set: ResultSet
path_settings = PathSettings(Path("beta-output-alpha/"))


def log_and_print(message: str):
    print(message)
    log_function(log_filename, message)


def init_exec():
    global _main_ui, _mw, _app
    _main_ui = view_main_window.Ui_MainWindow()
    _app = QtWidgets.QApplication(sys.argv)
    _mw = MainWindow()
    _main_ui.setupUi(_mw)
    sys.excepthook = exception_handler

    _setup_listeners()
    _mw.show()
    _set_executing_status(False, False)
    _update_status_text(lp.program_ready)
    return _app.exec()


def _open_file():
    global _main_ui
    dialog = QtWidgets.QFileDialog()
    a = dialog.getOpenFileName(filter='(*.tiff *.tif)')
    log_and_print(f"User selected a file: {a}")
    if a[0] != '':
        _load_image_info(Path(a[0]))
        _update_info()
        _set_executing_status(is_executing=False, is_initialized=True)
    else:
        log_and_print("The selected file was ignored")


def _load_image_info(tiff_path: Path):
    global geo_transform_info
    log_and_print("loading tiff info")
    geo_transform_info = GeoTransformer(tiff_path)
    path_settings.set_input_file(tiff_path)
    _update_status_text(f"{tiff_path.name} loaded")


def can_exit() -> bool:
    # todo: check whether program is running, and exit if either not running or user abort execution
    log_and_print(lp.exit_program)
    return True


def _setup_listeners():
    """
    I am so used to java that I forgot it is slots and signal
    """

    _main_ui.pushButton_source.clicked.connect(_open_file)
    _main_ui.actionSelect_Source.triggered.connect(_open_file)
    _main_ui.pushButton_startStopProcessing.clicked.connect(worker_doing_its_job)
    _main_ui.actionExit.triggered.connect(_app.quit)
    _main_ui.tableView_results.selectionChanged = on_table_selection_changed

    _main_ui.pushButton_exportTable.clicked.connect(tester_result_display)


# def _setup_cropper(output_path: Path):
#     # todo: add parameters
#     global cropper
#     # print("setting up image cropper")
#     # print(cropper)
#     cropper = ImageCropperReversed(
#         crop_width=256, crop_height=256,
#         output_folder=output_path,
#         output_format=SaveFormat.JPG
#     )
#     cropper.update_progress_callback.connect(_update_status)


def _on_result_return():
    # _display_result(results)
    # _complete_execution_cleanup(lp.execute_complete, _display_result, results)
    _set_executing_status(is_executing=False, is_completed=True)
    global result_set, path_settings
    # print(path_settings.get_output_base_directory())  # debug
    result_set = ResultSet(path_settings)
    result_set.setup_list()
    _main_ui.tableView_results.setModel(result_set)
    _update_status_text(lp.execute_complete)


def _complete_execution_cleanup(message: str, func: callable, *args):
    """
    more like a place for re-enabling widget, displaying status message, and execute a finalize function (callback)
    """
    _set_executing_status(is_executing=False, is_completed=True)
    if func is not None:
        func(*args)
    if message is not None and message != '':
        _update_status_text(message)


def worker_doing_its_job():
    """
    create(hire) a worker thread and run it (no salary)
    """
    _set_executing_status(True)
    assert geo_transform_info is not None, "GeoTransformer is required to execute"
    # assert cropper is not None  # moved cropper set up to worker's job

    # image enhance = 40%, image cropping = 10%, image recognize = 40%, result load = 10%
    progress = 0
    enable_cuda = _main_ui.checkBox_enableCUDA.isChecked()
    enhance_image = _main_ui.checkBox_enableImageEnhance.isChecked()
    clean_up_output_path = True
    global worker_thread
    worker_thread = WorkerSJob(enable_cuda=enable_cuda, enhance_image=enhance_image,
                               clean_up_output_path=clean_up_output_path)
    worker_thread.job_complete_signal.connect(
        # lambda: _complete_execution_cleanup(lp.execute_complete, _main_ui.tableView_results.setModel, result_set)
        # lambda result: _complete_execution_cleanup(lp.execute_complete, _display_result, result)
        _on_result_return
    )
    worker_thread.job_progress_update_signal.connect(_update_status)
    # todo: worker_thread.job_abort_signal
    worker_thread.start()


def worker_stopping_its_job():
    """
    not going to make a stop button
    """
    pass


# FUNCTIONS THAT UPDATES GUI: SHOW INFO, ENABLE & DISABLE CONTROL


def _update_info():
    if path_settings.source.exists() and path_settings.source.is_file():
        log_and_print("updating tiff info")
        _main_ui.label_source_content.setText(path_settings.source.name)
        _main_ui.label_imageInfo_filepath_content.setText(path_settings.source.__str__())
        _main_ui.label_imageInfo_resolution_content.setText(
            geo_transform_info.get_resolution_str()
        )
        _main_ui.label_imageInfo_cc_tl_content.setText(geo_transform_info.tl.__str__())
        _main_ui.label_imageInfo_cc_tr_content.setText(geo_transform_info.tr.__str__())
        _main_ui.label_imageInfo_cc_bl_content.setText(geo_transform_info.bl.__str__())
        _main_ui.label_imageInfo_cc_br_content.setText(geo_transform_info.br.__str__())
        log_and_print("updated tiff info")


def _display_result(results: ResultSet):
    # get result data from WorkerSJob, and load it to table view
    global result_set
    result_set = results
    _main_ui.tableView_results.setModel(result_set)


def _update_status_text(message: str):
    if message is not None and message != '':
        _main_ui.label_status.setText(message)
        log_and_print(message)


def _update_progress_bar(progress: int):
    if progress is not None:
        _main_ui.progressBar_status.setValue(progress)


def _update_status(message: str, progress: int):
    # print(f"{message}, {progress}")
    _update_status_text(message)
    _update_progress_bar(progress)


def _set_executing_status(is_executing: bool = worker_thread_running, is_initialized: bool = True,
                          is_completed: bool = False):
    global worker_thread_running
    worker_thread_running = is_executing
    _main_ui.pushButton_startStopProcessing.setEnabled(not is_executing and is_initialized)
    if is_executing:
        _main_ui.progressBar_status.setValue(0)
    _main_ui.pushButton_source.setEnabled(not is_executing)
    _main_ui.tab_config.setEnabled(not is_executing)
    _main_ui.tab_result.setEnabled(not is_executing and is_completed)
    _main_ui.progressBar_status.setVisible(is_executing)


# FUNCTION FOR DEVELOPMENT (UNUSED AFTER RELEASE)

# # test_result_display
class TestResultDisplay(QtCore.QThread):
    finish_signal = pyqtSignal()

    def run(self):
        src = Path(
            "source/extracted/GF1_PMS1_E122.3_N30.5_20210501_L1A0005630448/GF1_PMS1_E122.3_N30.5_20210501_L1A0005630448-MSS1.tiff")
        result_dir = PathSettings(Path("beta-output/"), source=Path('.1.txt'))
        global geo_transform_info, path_settings
        path_settings = result_dir
        geo_transform_info = GeoTransformer(src)
        self.finish_signal.emit()
        # result_set.test_signal.connect(_update_status)  # test&debug


tester_result_display_thread: TestResultDisplay


def tester_result_display():
    _set_executing_status(True)
    _update_status_text("Loading results")
    global tester_result_display_thread
    tester_result_display_thread = TestResultDisplay()
    tester_result_display_thread.finish_signal.connect(_on_result_return)
    tester_result_display_thread.start()


# # QThreadExceptionTest
class QThreadExceptionTest(QtCore.QThread):
    """
    exception from a thread can be handled by main thread
    """
    error_signal = pyqtSignal(Exception)

    def run(self):
        try:
            assert False, "raise a assertion exception"
        except Exception as ex:
            self.error_signal.emit(ex)


q_thread_exception_tester: QThreadExceptionTest


def q_thread_exception_test():
    global q_thread_exception_tester
    q_thread_exception_tester = QThreadExceptionTest()
    q_thread_exception_tester.error_signal.connect(raise_exception_in_main_thread)
    try:
        q_thread_exception_tester.start()
    except Exception as ex:
        print("the catch after .start()")
        raise_exception_in_main_thread(ex)



if __name__ == "__main__":
    # b = MainWindowController()
    sys.exit(init_exec())
    # print('sleeping')
    # time.sleep(10)
    # print('continue')
    # sys.exit(execute())
log_and_print(f"Working directory: {os.getcwd()}")
