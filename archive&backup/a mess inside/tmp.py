import os
import glob
from shutil import copyfileobj

tasks_dir = r"E:\Shipdataset"
save_dir = r"E:\rs_shipdet\yolox-pytorch-main\VOCdevkit\VOC2007"
tasks_path = glob.glob(tasks_dir + "/*")
count = 0

def copyfile(src, dst):
    with open(src, 'rb') as fsrc:
        with open(dst, 'wb') as fdst:
            copyfileobj(fsrc, fdst)
    fdst.close()
    fsrc.close()

for task_path in tasks_path:
    origin_xml_dir = os.path.join(task_path, "xml")
    origin_img_dir = os.path.join(task_path, "img")
    names = os.listdir(origin_xml_dir)
    for name in names:
        name = name[:-4]
        src_xml_path = os.path.join(origin_xml_dir, name + '.xml')
        src_img_path = os.path.join(origin_img_dir, name + '.jpg')

        new_name = f"%05d"%count

        dst_xml_path = os.path.join(save_dir, "Annotations", new_name + '.xml')
        dst_img_path = os.path.join(save_dir, "JPEGImages", new_name + '.jpg')
        copyfile(src_xml_path, dst_xml_path)
        copyfile(src_img_path, dst_img_path)
        count+=1
