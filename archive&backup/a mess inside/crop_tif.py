from enum import auto

from tifffile import tifffile, TiffPage
import numpy as np

"""
from: https://gist.github.com/rfezzani/b4b8852c5a48a901c1e94e09feb34743
this is supposed to be a fork (but I donno how to do that without using github)
"""


class SaveFormat:
    PNG: auto
    TIFF: auto


def tif_convert_pypng_ver2(page: TiffPage,
                           start_width: int,
                           start_height: int,
                           crop_size_width: int,
                           crop_size_height: int,
                           bypass_error: bool = True) -> np.array:
    height, width, channels = page.shape
    if start_height < 0 or \
            start_height + crop_size_height > height \
            or start_width < 0 or \
            start_width + crop_size_width > width:
        if bypass_error:
            start_height = min(max(0, start_height), height-1)
            start_width = min(max(0, start_width), width-1)
            crop_size_height = height - start_height - 1
            crop_size_width = width - start_width - 1
        else:
            raise ValueError("Requested crop area is out of image bounds.")

    test = page.asarray('memmap')  # use memmap to avoid memory overflow, especially for really large tiff image

    # tiff image for the project I'm involved shapes (height, width, channel)
    save = test[start_height:start_height + crop_size_height, start_width:start_width + crop_size_width, :]

    save_array = []
    for row in save:
        r = []
        for column in row:
            for element in column:
                r.append(element)
        save_array.append(r)
    return np.array(save_array)




def get_fucking_crop(page, x0, y0, h, w, restrict_upper_bound: bool = False):
    """Extract a crop from a TIFF image file directory (IFD).

    Only the tiles englobing the crop area are loaded and not the whole page.
    This is usefull for large Whole slide images that can't fit int RAM.

    implementation should include the lower bound but omit the upper bound.
    e.g. x0 starts at 0, with width(w) set to 5, result in x1 equals to 4. [0,4]==[0,5)

    Parameters
    ----------
    page : TiffPage
        TIFF image file directory (IFD) from which the crop must be extracted.
    x0, y0: int
        Coordinates of the top left corner of the desired crop.
    h: int
        Desired crop height.
    w: int
        Desired crop width.
    restrict_upper_bound: bool
        Instead of raising an area out of bound error, use image upper bound to crop

    Returns
    -------
    out : ndarray of shape (imagedepth, h, w, sampleperpixel)
        Extracted crop.

    """

    x1, y1 = x0 + w, y0 + h  # bottom right corner of the desired crop
    tags = page.tags
    image_width, image_height = page.imagewidth, page.imagelength
    print(f'{image_width}x{image_height}')
    if restrict_upper_bound:
        x1 = min(x0 + w, image_width)
        y1 = min(y0 + h, image_height)

    if h < 1 or w < 1:
        raise ValueError("h and w must be strictly positive.")

    if x0 < 0 or y0 < 0 or x0 + h >= image_height or y0 + w >= image_width:
        raise ValueError("Requested crop area is out of image bounds.")

    out = np.empty((page.imagedepth, h, w, page.samplesperpixel),
                   dtype=page.dtype)

    fh = page.parent.filehandle

    jpegtables = page.tags.get('JPEGTables', None)
    if jpegtables is not None:
        jpegtables = jpegtables.value

    for i in range(x0, x1):
        for j in range(y0, y1):
            index = j * image_width + i

            offset = page.dataoffsets[index]
            byte_count = page.databytecounts[index]

            fh.seek(offset)
            data = fh.read(byte_count)
            pixel, indices, shape = page.decode(data, index, jpegtables)  # renamed to pixel because it's all I got

            image_x = i - x0
            image_y = j - y0
            out[:, image_y, image_x, :] = pixel

    return out


def get_crop(page, i0, j0, h, w):
    """Extract a crop from a TIFF image file directory (IFD).

    Only the tiles englobing the crop area are loaded and not the whole page.
    This is usefull for large Whole slide images that can't fit int RAM.

    Parameters
    ----------
    page : TiffPage
        TIFF image file directory (IFD) from which the crop must be extracted.
    i0, j0: int
        Coordinates of the top left corner of the desired crop.
    h: int
        Desired crop height.
    w: int
        Desired crop width.

    Returns
    -------
    out : ndarray of shape (imagedepth, h, w, sampleperpixel)
        Extracted crop.

    """

    if not page.is_tiled:
        raise ValueError("Input page must be tiled.")

    im_width = page.imagewidth
    im_height = page.imagelength

    if h < 1 or w < 1:
        raise ValueError("h and w must be strictly positive.")

    if i0 < 0 or j0 < 0 or i0 + h >= im_height or j0 + w >= im_width:
        raise ValueError("Requested crop area is out of image bounds.")

    tile_width, tile_height = page.tilewidth, page.tilelength
    i1, j1 = i0 + h, j0 + w

    tile_i0, tile_j0 = i0 // tile_height, j0 // tile_width
    tile_i1, tile_j1 = np.ceil([i1 / tile_height, j1 / tile_width]).astype(int)

    tile_per_line = int(np.ceil(im_width / tile_width))

    out = np.empty((page.imagedepth,
                    (tile_i1 - tile_i0) * tile_height,
                    (tile_j1 - tile_j0) * tile_width,
                    page.samplesperpixel), dtype=page.dtype)

    fh = page.parent.filehandle

    jpegtables = page.tags.get('JPEGTables', None)
    if jpegtables is not None:
        jpegtables = jpegtables.value

    for i in range(tile_i0, tile_i1):
        for j in range(tile_j0, tile_j1):
            index = int(i * tile_per_line + j)

            offset = page.dataoffsets[index]
            bytecount = page.databytecounts[index]

            fh.seek(offset)
            data = fh.read(bytecount)
            tile, indices, shape = page.decode(data, index, jpegtables)

            im_i = (i - tile_i0) * tile_height
            im_j = (j - tile_j0) * tile_width
            out[:, im_i: im_i + tile_height, im_j: im_j + tile_width, :] = tile

    im_i0 = i0 - tile_i0 * tile_height
    im_j0 = j0 - tile_j0 * tile_width

    return out[:, im_i0: im_i0 + h, im_j0: im_j0 + w, :]
