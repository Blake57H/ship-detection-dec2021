import os
import platform
import shutil
import sys
import warnings
from datetime import datetime
from pathlib import Path


class LanguagePack:
    program_ready = "Ready to select source"
    execute_ready = "Ready to run"
    executing = "Running"
    execute_complete = "Completed"

    read_image = "Reading image"
    enhance_image = "Enhancing image"
    crop_image = 'Creating image segments'
    detecting_ships = 'Recognizing ships from image segments'
    displaying_result = "Loading detection result"

    error_popup_title = "An error has occurred"
    error_popup_body = "An error has occurred and program needs to restart:"

    pushBtnStart = "Start"
    pushBtnStop = "Stop"

    exit_program = "Program Terminating"

    def load_translation(self, file):
        """
        future function perhaps
        """
        pass

    def get_error_popup_body(self, text):
        return self.error_popup_body + "\n" + text


def show_warning(message: str):
    # warnings.showwarning = _warning
    # warnings.warn(message)
    warnings.warn_explicit(message, UserWarning, '', -1)


def clean_temporary_folder(specific_directory: str):
    """
    Delete all folder and files in a directory (used to clear temporary files)
    You are allowed to delete other things as well
    :param specific_directory: the folder you wanna clean
    :return: nothing
    """
    if not os.path.exists(specific_directory):
        return
    assert os.path.isdir(specific_directory), f"{specific_directory} is not a directory"
    for filename in os.listdir(specific_directory):
        file_path = os.path.join(specific_directory, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))


def log_function(log_file: Path, message):
    if not log_file.parent.exists():
        log_file.parent.mkdir(parents=True)
    with open(log_file.__str__(), 'a') as stream:
        stream.write(f"[{datetime.now().__str__()}] ")
        stream.writelines(message)
        stream.writelines('\n')


def get_time_as_filename() -> str:
    time = datetime.now()
    return f"{time.year}-{time.month}-{time.day}-{time.hour}-{time.minute}-{time.second}-{time.microsecond}"
