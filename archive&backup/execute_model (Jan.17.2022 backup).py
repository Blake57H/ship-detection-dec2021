import codecs
import numpy as np
import os

from PyQt6.QtCore import pyqtSignal, QObject
from tqdm import tqdm
from yolo import YOLO
import glob
from PIL import Image

model_infors = {
    "yolo_m": {
        "phi": "m",
        "model_path": 'model_output/yolo_m_best.pth',
        "input_shape": [640, 640],
    },
}


def model_select(name):
    return model_infors[name]


def detect(task_path, model_name, cfg):
    model_info = model_select(model_name)

    task_pic_path = os.path.join(task_path, "tmps")
    task_dir = os.listdir(task_pic_path)

    sub_save_imgdir = os.path.join(task_path, "img_results")
    sub_save_txtdir = os.path.join(task_path, "txt_results")
    if not os.path.exists(sub_save_imgdir):
        os.makedirs(sub_save_imgdir)
    if not os.path.exists(sub_save_txtdir):
        os.makedirs(sub_save_txtdir)

    pbar = tqdm(range(len(task_dir)))

    yolo = YOLO(model_info, cfg)

    progress_total = len(task_dir)
    progress = 0

    for sub_task in task_dir:
        pbar.update(1)

        img_path = os.path.join(task_pic_path, sub_task)
        # print(f"{sub_task} processing...")

        img_name = sub_task.split(".")[0]
        img_save_path = os.path.join(sub_save_imgdir, img_name + '.jpg')
        txt_save_path = os.path.join(sub_save_txtdir, img_name + '.txt')
        img = Image.open(img_path)
        try:
            draw_image, txt_list = yolo.detect_image(img)
            draw_image.save(img_save_path)
            filestream = codecs.open(txt_save_path, 'w', 'utf-8')
            for i in range(len(txt_list)):
                readline = txt_list[i]
                filestream.write(readline)
            filestream.close()
        except Exception as e:
            print("Get Error : %s" % e)
            img.save(img_save_path)
            codecs.open(txt_save_path, 'w', 'utf-8')
        # print(f"{sub_task} done!")

    return True


class DetectModel(QObject):
    update_progress_signal = pyqtSignal(int)

    def __init__(self, enable_cuda: bool = False):
        super().__init__()
        self.config = {
            "classes": ["ship"],
            "confidence": 0.5,
            "nms_iou": 0.3,
            "cuda": enable_cuda
        }
        self.model_info = model_select("yolo_m")

    def run(self, input: str, image_output: str, text_output: str):
        model_info = self.model_info

        task_pic_path = input
        task_dir = os.listdir(task_pic_path)

        sub_save_imgdir = image_output
        sub_save_txtdir = text_output
        if not os.path.exists(sub_save_imgdir):
            os.makedirs(sub_save_imgdir)
        if not os.path.exists(sub_save_txtdir):
            os.makedirs(sub_save_txtdir)

        progress_total = len(task_dir)
        pbar = tqdm(range(progress_total))
        # print("ENTER MAIN LOOP")  # debug

        yolo = YOLO(model_info, self.config)
        # print("YOLO LOADED")  # debug

        # print(progress_total)  # debug
        progress = 0

        for sub_task in task_dir:
            pbar.update(1)

            progress += 1
            self.update_progress_signal.emit(int(progress * 100 / progress_total))

            img_path = os.path.join(task_pic_path, sub_task)
            # print(f"{sub_task} processing...")

            img_name = sub_task.split(".")[0]
            img_save_path = os.path.join(sub_save_imgdir, img_name + '.jpg')
            txt_save_path = os.path.join(sub_save_txtdir, img_name + '.txt')
            img = Image.open(img_path)
            try:
                draw_image, txt_list = yolo.detect_image(img)
                draw_image.save(img_save_path)
                filestream = codecs.open(txt_save_path, 'w', 'utf-8')
                for i in range(len(txt_list)):
                    readline = txt_list[i]
                    filestream.write(readline)
                filestream.close()
            except Exception as e:
                print("Get Error : %s" % e)
                img.save(img_save_path)
                codecs.open(txt_save_path, 'w', 'utf-8')
            # print(f"{sub_task} done!")


if __name__ == "__main__":
    task_path = "E:/rs_shipdet/yolox-pytorch-main/tasks/test_20220106173402"
    model_name = "yolo_m"
    model_info = model_select(model_name)
    cfg = {
        "classes": ["ship"],
        "confidence": 0.5,
        "nms_iou": 0.3,
        "cuda": True
    }
    if detect(task_path, model_info, cfg):
        print("all tasks done!")
